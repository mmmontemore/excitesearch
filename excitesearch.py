import sys
import json
from json import JSONDecodeError
import math
import os
import time
import webbrowser
import sqlite3
import scipy.cluster.hierarchy as sch
from sklearn.cluster import AgglomerativeClustering
try:
    import pandas as pd
except ImportError:
    print('We need the pandas package. To download it, open a bash window, type \'pip3 install pandas\' and hit enter.')
    sys.exit(1)
try:
    import matplotlib.pyplot as plt
except ImportError:
    print("We need the matplotlib package. To download it, open a bash window, type \'pip3 install matplotlib\' and hit enter.")
    sys.exit(1)
try:
    import requests
except ImportError:
    print("We need the requests package. To download it, open a bash window, type \"pip3 install requests\" and hit enter.")
    sys.exit(1)
try:
    import networkx as nx
except ImportError:
    print("We need the networkx package. To download it, open a bash window, type \"pip3 install networkx\", and hit enter.")
    sys.exit(1)
try:
    from crossref.restful import Works
except ImportError:
    print("We need Crossref. To download it, open a bash window, type \"pip3 install crossrefapi\", and hit enter.")
    sys.exit(1)
import nltk
#nltk.download('stopwords')
try:
    from nltk.corpus import stopwords 
    from nltk.tokenize import word_tokenize
    #nltk.download('punkt')
except ImportError:
    nltk.download('punkt')
    print("We need the nltk package. To download it, open a bash window, \"type pip3 install nltk \", and hit enter.")
    sys.exit(1)
try:
    import numpy as np
except ImportError:
    print("We need NumPy. To download it, open a bash window, type \"pip3 install numpy\", and hit enter.")
    sys.exit(1)

from requests.exceptions import SSLError
from requests.exceptions import ReadTimeout
from itertools import chain
from termcolor import colored

class SciPaper(object):
    """A dictionary-like class representing scientific papers.
        Functions:
            add_to_database()
            get_data()
    """
    def __init__(self, doi, attrs=None):
        '''Constructor.
            Args:
                doi (str): DOI of article represented
            Kwargs:
                attrs (dict): Metadata
        '''
        self.attrs = None
        if attrs != None:
            self.attrs = attrs
        else:
            self.attrs = {
                'DOI': doi,
                'title':None,
                'author':None, #author will be of type list even if there is only one
                'year':None,
                'abstract':None,
                'journal':None,
                'citations':[]
                }

    def add_to_database(self):
        '''Checks to see if a SciPaper is already in the database. If not, it adds it.
            Args: None 
            Returns: None
        '''
        temp = self.attrs['DOI']
        corpus = Corpus()
        if corpus.get_entry(temp) == None:
            temp = dict(self.attrs)
            temp.pop('citations')
            corpus.add_entry(temp)
            n = Network(self)
        else:
            md = corpus.get_entry(self.attrs['DOI'])
            if len(self.attrs['citations']) == 0:
                n = Network()
                refs = n.get_refs(self.attrs['DOI'])
                md['citations'] = refs
                n = Network(self)

    def get_data(self,warnings=True):
        '''Retrieves metadata about a SciPaper.
            Args: warnings=True(Bool): if False, will not print warnings.
            Returns: self.attrs'''
        if self.attrs['title'] != None and self.attrs['author'] != None and self.attrs['year'] != None and self.attrs['journal'] != None and self.attrs['abstract'] != None and self.attrs['citations'] != None:
            return None
        corpus = Corpus()
        ent = corpus.get_entry(self.attrs['DOI'])
        if ent != None:
            met = ent
            if len(met['citations']) == 0:
                n = Network()
                met['citations'] = n.get_refs(self.attrs['DOI'])
            return met
        
        #Crossref
        cr = True
        works = Works()
        try:
            p = works.doi(self.attrs['DOI'])
            if p == None:
                cr = False
        except JSONDecodeError:
            pass
            cr = False
        if cr:
            try:
                self.attrs['title'] = p.get('title')[0]
            except IndexError:
                pass
            self.attrs['author'] = p.get("author")
            try:
                self.attrs['year'] = p["published-online"].get("date-parts")[0][0]
            except KeyError:
                pass
                try:
                    self.attrs['year'] = p['published-print'].get('date-parts')[0][0]
                except KeyError:
                    pass
            self.attrs['abstract'] = p.get("abstract")
            self.attrs['journal'] = p.get('container-title')[0]
            if len(self.attrs['citations']) > 0:
                ref_list = p.get("reference")
                if ref_list != None:
                    for i in range(len(ref_list)):
                        d = ref_list[i].get("DOI")
                        if d != None:
                            self.attrs["citations"].append(d)
            if len(self.attrs["author"]) > 1: #corrects formatting of author(s) from JSON
                temp = [] #multiple authors
                for i in range(len(self.attrs["author"])):
                    first = self.attrs["author"][i].get("given")
                    last = self.attrs["author"][i].get("family")
                    temp.append((first + " " + last))
                self.attrs["author"] = temp #author is of type list 
            elif len(self.attrs["author"]) == 1: #single author
                first = self.attrs["author"][0].get("given")
                last = self.attrs["author"][0].get("family")
                self.attrs["author"] = [first + " " + last]
                self.attrs["author"] = None
        else:
            cr_url = 'https://api.crossref.org/works/' + self.attrs['DOI']
            r = requests.get(cr_url)
            p = None
            page = r.text
            if page != "Resource not found." and page != '[]':
                try:
                    info = json.loads(r.text)
                    if type(p) != str:
                        p = 1 #so p != None and next block doesn't execute
                except JSONDecodeError:
                    pass
                try:
                    temp = info['message']['reference']
                    for t in temp:
                        try:
                            self.attrs['citations'].append(t['DOI'])
                        except KeyError:
                            pass
                except KeyError:
                    pass
                    p = None
                except TypeError:
                    pass
                    p = None
                self.attrs['title'] = info['message']['title'][0]
                self.attrs['journal'] = info['message']['short-container-title'][0] #gets journal
                try: #gets year
                    self.attrs['year'] = info['message']['published-print']['date-parts'][0][0]
                except KeyError:
                    pass
                    try:
                        self.attrs['year'] = info['message']['published-online']['date-parts'][0][0]
                    except KeyError:
                        pass
                try:
                    temp = page['message']['author'] #gets author(s) - will be of type list
                except TypeError:
                    pass
                if len(temp) == 1:
                    self.attrs['author'] = [temp[0]['given'] + ' ' + temp[0]['family']]
                else:
                    self.attrs['author'] = []
                    for t in temp:
                        self.attrs['author'].append((t['given'] + ' ' + t['family']))

            if p == None:
                cr = False
        if cr == False:
            oc_base_url = 'https://opencitations.net/index/api/v1/metadata/'
            try:
                r = requests.get((oc_base_url+self.attrs['DOI']))
            except ReadTimeout:
                pass
                jso = False
            except TypeError:
                pass
                jso = False
            try:
                t = r.text
                jso = True
            except UnboundLocalError:
                pass
                jso = False
            try:
                metadata = json.loads(t)[0]
            except JSONDecodeError:
                pass
                jso = False
            except IndexError:
                pass
                jso = False
            if jso:
                self.attrs['title'] = metadata['title']
                self.attrs['year'] = metadata['year']
                self.attrs['journal'] = metadata['source_title']
                authors = metadata['author'].split('; ')
                for k in range(len(authors)):
                    a = authors[k].split(', ')
                    try:
                        authors[k] = a[1] + ' ' + a[0]
                    except IndexError:
                        pass
                        authors[k] = a
                self.attrs['author'] = authors
                if len(self.attrs['citations']) > 0:
                    temp = metadata['reference'].split('; ')
                    while '' in temp:
                        temp.remove('')
                    self.attrs['citations'] = temp

            if jso == False and warnings == True: #no info
                print('Warning: Metadata for this DOI could not be found. Please check to ensure that it is spelled correctly.')
                return None

            
        ss_base_url = 'https://api.semanticscholar.org/graph/v1/paper/'
        if self.attrs['abstract'] == None:
            ss_url = ss_base_url + self.attrs['DOI'] + '?fields=abstract'
            r = requests.get(ss_url)
            page = r.text
            info = json.loads(page)
            self.attrs['abstract'] = info['abstract']
            
        self.add_to_database()
        return self.attrs
        
class Corpus(object):
    """Representation of ExCiteSearch's corpus. Upon initialization, writes itself into the
    database (data.txt) and adds all the DOIs in it into the file dois.txt
    """
    def __init__(self):
        '''Constructor
        '''
        self.con = sqlite3.connect('data/corpus.db')
    
    def add_entry(self, attrs):
        '''Adds entry to corpus.
        Args:
            attrs (dict): dictionary of attributes, see SciPaper attrs
        Returns:
            None
        '''
        cursorObj = self.con.cursor()

        attrs['author'] = json.dumps(attrs['author'])
        
        entities = list(attrs.values())
        exe = 'INSERT INTO articles(doi, title, author, year, abstract, journal) VALUES(?, ?, ?, ?, ?, ?)'

        cursorObj.execute(exe, entities)
        
        self.con.commit()
        
    def add_from_wos(self, filename):
        '''Adds in data from Web of Science.
            Args:
                filename(str): Path to file with data in it
            Returns: None'''
        jo = open(filename, 'r')
        journal_data = jo.read()
        jo.close()
        con = sqlite3.connect('data/corpus.db')
        while len(journal_data) > 5:
            i = journal_data.index("AF")
            j = journal_data.index("ER\n\n")
            chunk = journal_data[i:j].split()
            doi = ''
            references = []
            title = ''
            abstract = ''
            journal = ''
            authors = []
            year = 0
            c = 1
            if 'DI' in chunk and len(chunk) > 1:
                doi = chunk[chunk.index('DI') + 1]
                if (doi+'\n') not in self.dois: #makes sure it's not already in the database
                    while c < chunk.index('TI'): #authors
                        first = ''
                        last =  ''
                        if ',' in chunk[c]:
                            last = chunk[c][:-1]
                            first = chunk[c+1]
                            authors.append(first + ' ' + last)
                        c = c + 1
                    if 'TI' in chunk: #makes sure it has title 
                        c = chunk.index('TI') + 1
                        while c < chunk.index('SO'): #title
                            title = title + ' ' + chunk[c]
                            c = c + 1
                        c = c + 1
                    else:
                        c = chunk.index('SO') + 1
                    if 'LA' in chunk:
                        while c < chunk.index('LA'):#journal
                            journal = journal + chunk[c] + ' '
                            c = c + 1
                    else:
                        while c < chunk.index('AB'):
                            journal = journal + chunk[c] + ' '
                            c = c + 1
                    if 'AB' in chunk: #makes sure abstract is present
                        c = chunk.index('AB') + 1
                        if ('C1') in chunk:
                            while c < chunk.index('C1'): #gets abstract
                                abstract = abstract + chunk[c] + ' '
                                c = c + 1
                        elif ('RP') in chunk:
                            while c < chunk.index('RP'): #gets abstract
                                abstract = abstract + chunk[c] + ' '
                                c = c + 1
                        elif ('Published ') in chunk:
                            while c < chunk.index('Published '):
                                abstract = abstract + chunk[c] + ' '
                                c = c + 1
                    if 'CR' in chunk:
                        c = chunk.index('CR') + 1
                        while c < chunk.index('DI'):#citations
                            if chunk[c] == 'DOI' and chunk[c+1][0:3] == '10.':
                                references.append(chunk[c+1])
                            c = c + 1
                    if 'DA' in chunk: #gets year
                        c = chunk.index('DA') + 1
                        year = chunk[c][0:4] 
                    if '10.' in doi: #makes sure it's an actual DOI
                        sp = SciPaper(doi)
                        sp.attrs['citations'] = references #adds data to SciPaper's attrs
                        if title != '':
                            sp.attrs['title'] = title
                        if abstract != '':
                            sp.attrs['abstract'] = abstract
                        if journal != '':
                            sp.attrs['journal'] = journal
                        if year != 0:
                            sp.attrs['year'] = year
                        if len(authors) != 0:
                            sp.attrs['author'] = authors
                        
                        self.add_entry(sp.attrs)
            journal_data = journal_data[j+1:] #moves on

    def get_entry(self, doi):
        '''
        Args:
            doi (str): DOI
        Returns:
            row (dict): see SciPaper attrs
        '''
        cursorObj = self.con.cursor()
        query = "SELECT * FROM articles WHERE doi IS '" + doi + "'"
        row = self.con.execute(query).fetchone()
        if row == None:
            return None
        att = ['DOI','title','author','year','abstract','journal','citations']
        row = dict(zip(att, row))
        row['author'] = json.loads(row['author'])
        return row

    def edit_entry(self, doi, attr):
        '''
        Args:
            doi (str): DOI of entry to be updated
            attr (tup): tuple where first element is tag of attribute to be changed, second element
                    is what attribute will be changed to
        Returns: None
        '''
        cursorObj = self.con.cursor()

        att = attr[1]
        if type(att) == list:
            att = [el.replace("'", "") for el in att]
            att = json.dumps(att)
        elif type(att) == int:
            att = str(int)
        elif type(att) == str:
            att.replace("'", "")
        elif att == None:
            att = 'None'

        exe = "UPDATE articles SET '" + attr[0] + "' = '" + att + "' WHERE doi IS '" + doi + "'"
        
        cursorObj.execute(exe)

        self.con.commit()

class Network(object):
    """Creates a graphical representation of a SciPaper and citations using the networkx package.
    Attributes: self.graph (NetworkX Graph): graph of citation data
    functions:
        __init__ - constructor - sets up graph.
        search - returns all DOIs within a certain citation distance of a SciPaper
        common_cit - returns all DOIs within a certain distance of two papers
    """
    def __init__(self, update=None):
        '''Constructor.
            update is False when the graph needs to be initialized.
            update is SciPaper when the graph needs to be updated.
            update is None when the graph does not need to be updated or initialized.
        '''
        self.graph = None
        if update == None: #graph does not need to be updated or initialized
            try:
                df = pd.read_csv(filepath_or_buffer='data/network.csv')
            except FileNotFoundError:
                pass
                try:
                    df = pd.read_csv(filepath_or_buffer=(os.getcwd() + '/data/network.csv'))
                except FileNotFoundError:
                    pass
                    p = os.getcwd()
                    df = pd.read_csv(filepath_or_buffer=p+'/excitesearch/data/network.csv')
            self.graph = nx.from_pandas_edgelist(df)
        elif type(update) == SciPaper: #graph needs to be updated
            try:
                df = pd.read_csv(filepath_or_buffer='data/network.csv')
            except FileNotFoundError:
                pass
                p = os.getcwd()
                df = pd.read_csv(filepath_or_buffer=p+'/excitesearch/data/network.csv')
            self.graph = nx.from_pandas_edgelist(df)
            nodes = self.graph.nodes
            self.graph.add_node(update.attrs['DOI'])
            self.graph.add_nodes_from(update.attrs['citations'])
            for i in range(len(update.attrs['citations'])):
                self.graph.add_edge(update.attrs['DOI'], update.attrs['citations'][i])
            self.update()

    def update(self):
        '''Updates graph that is saved to .csv file.
            Args: None
            Returns: None
        '''
        df_new = nx.to_pandas_edgelist(self.graph)
        p = os.getcwd().lower()
        if 'excitesearch' in p:
            df_new.to_csv(path_or_buf='data/network.csv')
        else:
            o = os.path.dirname(os.path.abspath('excitesearch.py'))
            df_new.to_csv(path_or_buf=o+'/excitesearch/data/network.csv')

    def get_refs(self, doi):
        '''Obtains references from either OpenCitations or Crossref.
            Args: doi (str or list): either a single DOI or a list of DOIs
            Returns: citations/lst (list): list of DOIs
        '''
        if doi in self.graph.nodes:
            li = list(self.graph.neighbors(doi))
            if len(li) > 1:
                return li
        citations = []
        oc_base_url = 'https://opencitations.net/index/api/v1/metadata/'
        if type(doi) == str:
            cr_url = 'https://api.crossref.org/works/' + doi
            try:
                r = requests.get(cr_url)
            except ConnectionError:
                pass
                time.sleep(10)
                r = requests.get(cr_url)
            p = None
            page = r.text
            citations = []
            if page != "Resource not found." and page != '[]':
                try:
                    page = json.loads(r.text)
                    if type(p) != str:
                        p = 1 #so p != None and next block doesn't execute
                except JSONDecodeError:
                    pass
                try:
                    temp = page['message']['reference']
                    for t in temp:
                        try:
                            citations.append(t['DOI'])
                        except KeyError:
                            pass
                except KeyError:
                    pass
                    p = None
                except TypeError:
                    pass
                    p = None
            if p == None:#else:
                r = requests.get((oc_base_url+doi.lower()))
                t = r.text
                jso = True
                try:
                    metadata = json.loads(t)
                except JSONDecodeError:
                    pass
                    jso = False
                if jso:
                    try:
                        metadata = metadata[0]
                        citations = metadata['reference'].split('; ') + metadata['citation'].split('; ')
                        if '' in citations:
                            citations.remove('')
                    except IndexError:
                        pass
            return citations
        else:
            works = Works()
            for i in range(len(doi)):
                p = None
                try:
                    p = works.doi(doi[i])
                except SSLError:
                    pass
                if p != None:
                    ref = p.get("reference")
                    if ref != None:
                        for j in range(len(ref)):
                            d = ref[j].get("DOI")
                            if d != None:
                                citations.append(d)
                else:
                    url = oc_base_url + doi[i]
                    r = requests.get(url)
                    t = r.text
                    jso = True
                    metadata = None
                    try:
                        metadata = json.loads(t)
                    except JSONDecodeError:
                        pass
                        jso = False
                    if jso:
                        metadata = json.loads(t)
                        for i in range(len(metadata)):
                            if type(metadata[i]) == dict:
                                citations = citations + metadata[i]['reference'].split('; ') + metadata[i]['citation'].split('; ')
                        while '' in citations:
                            citations.remove('')
            lst = list(dict.fromkeys(citations))
            return lst
            
    def search(self,paper,distance,return_all=None, disregard=None,warnings=False,halves=False,pull=True):
        """Finds all the DOIs within a certain citation distance of a paper.
        Args: paper(SciPaper): the starting point
            distance(int): the citation distance
            return_all (None or True): if None, returns the DOIs that are a certain citation distance
                from the query. If True, returns all DOIs within that distance.
            disregard(None or list): list of DOIs to ignore in the search. if None, includes all.
            warnings=True(Bool): whether or not to raise warnings if citation info can't be found
            halves=False(Bool): whether to count 1.5 citations
            pull=True(Bool): whether or not to pull metadata from Internet
        Returns:
            papers(list): a list of dictionaries of all the DOIs within a certain citation distance
            with corresponding SciPaper objects
        """
        doi = paper.attrs['DOI']
        corpus = Corpus()
        if distance > 10 or distance < 0:
            if warnings:
                print('Distance must be between 1 and 10 inclusive.')
            return None
        if distance == 0:
            return doi
        temp = True
        ent = corpus.get_entry(doi)
        if ent != None and doi in self.graph.nodes:
            temp = list(self.graph.neighbors(doi))
            if len(temp) == 0:
                temp = False
            if distance == 1:
                if temp == False:
                    temp = []
                return [[doi],temp]
        else:
            temp = False
        if temp == False:
            if pull:
                temp = self.get_refs(doi)
            else:
                temp = None
        if doi.lower() not in self.graph.nodes:
            self.graph.add_node(doi.lower())
        if temp == None:
            if warnings:
                print('No citations could be retrieved for ' + doi + '. Please make sure that ' +
                          'it is spelled correctly or try loading in metadata from Web of Science.')
            return None
        if disregard != None: 
            for d in disregard:
                if d in temp:
                    temp.remove(d)
        else:
            disregard = []
        self.graph.add_nodes_from(temp)
        for l in range(len(temp)):
            self.graph.add_edge(doi, temp[l])
        dois = [[doi]]
        num = 1
        all_dois = [doi]
        dis = False
        if disregard != None:
            dis = True
        if not halves:
            while num <= distance:
                if temp != None:
                    dois.append([t for t in temp if (t not in all_dois and t not in disregard)])
                else:
                    dois.append([])
                dois[num] = list(set(dois[num])) 
                if num == distance:
                    return dois
                all_dois = all_dois + temp
                temp1 = temp
                temp = []
                not_in = []
                for i in range(len(temp1)):
                    if (temp1[i] + '\n') in lst:
                        temp = temp + list(self.graph.neighbors(temp1[i]))
                    else:
                        not_in.append(temp1[i])
                if len(not_in) >= 1:
                    self.graph.add_nodes_from(not_in)
                    for i in range(len(not_in)):
                        if pull:
                            refs = self.get_refs(not_in[i])
                        for j in range(len(refs)):
                            self.graph.add_node(refs[j])
                            self.graph.add_edge(not_in[i], refs[j])
                        temp = temp + refs
                num+=1
        else: #halves
            num = 0.5
            while num <= (distance+1):
                if num % 1 != 0:
                    dois.append([])
                    num = num + 0.5
                    continue
                num = int(num)
                if temp != None:
                    dois.append([t for t in temp if (t != doi and t not in disregard)])
                else:
                    dois.append([])
                    
                indnum = int(num/0.5) #list index
                
                dois[indnum] = list(set(dois[indnum])) #removes duplicates
                
                if num >= 2:#halves - moves things around
                    doubles = [el for el in dois[(indnum-2)] if el in dois[indnum]]
                    dois[indnum-2] = [el for el in dois[indnum-2] if el not in doubles]
                    dois[indnum] = [el for el in dois[indnum] if el not in doubles]
                    dois[indnum-3] = doubles

                if num == (distance + 1):
                    dois = dois[:-1] #gets rid of extra layer
                    break
                num = num + 0.5
                

                all_dois = all_dois + temp
                temp1 = temp
                temp = []
                not_in = []
                for i in range(len(temp1)):
                    if (temp1[i] + '\n') in lst:
                        temp = temp + list(self.graph.neighbors(temp1[i]))
                    else:
                        not_in.append(temp1[i])
                if len(not_in) >= 1:
                    self.graph.add_nodes_from(not_in)
                    for i in range(len(not_in)):
                        refs = self.get_refs(not_in[i])
                        for j in range(len(refs)):
                            self.graph.add_node(refs[j])
                            self.graph.add_edge(not_in[i], refs[j])
                        temp = temp + refs

        dois[distance] = list(set(dois[distance])) #removes duplicates
        self.update()
        if return_all == True:
            return dois
        elif len(dois[distance]) > 0:
            return dois[distance]              
        else:
            return None #returns None if list is empty

    def common_cit(self, p1, p2=None, distance=1,return_style='list', warnings=True):
        '''Gets the citations in common between two SciPapers.
            Args: p1 (SciPaper or list of strings) - one SciPaper or a list of DOIs
                p2 (SciPaper or None) - another SciPaper, or None if p1 is list
                distance (int) - max distance to search for both. must be between 1 and 10.
                return_style='list'(str) - how to return - either 'list' or 'tuples'
                warnings=True (Bool): if True, will print warnings.
            Returns: lst (list) - list of dois ranked by relevance or df (DataFrame) pandas DataFrame with citations in common
         '''
        if distance < 1 or distance > 10:
            return None
        if type(p2) == SciPaper:
            l1 = self.search(p1, distance,True)
            l2 = self.search(p2, distance,True)
            if l1 == None or l2 == None:
                if warnings:
                    if l1 == None:
                        print('No citations could be obtained for ' + p1.attrs['DOI'] + '. Make sure that it is spelled' +
                              ' correctly or try loading citation information in manually from Web of Science.')
                    if l2 == None:
                        print('No citations could be obtained for ' + p2.attrs['DOI'] + '. Make sure that it is spelled' +
                              ' correctly or try loading citation information in manually from Web of Science.')
                return None
            if distance == 1:
                lst = []
                for i in range(len(l1[1])):
                    try:
                        if l1[1][i] in l2[1]:
                            lst.append(l1[1][i])
                    except TypeError:
                        pass
            else:
                master1 = []
                master2 = []
                lst = []
                for i in range(1,len(l1)):
                    master1 = master1 + l1[i]
                    master2 = master2 + l2[i]
                both = [el for el in master1 if el in master2]
                return both
                for i in range(len(both)):
                    score = 0
                    for i in range(1, len(l1)):
                        if both[i] in l1[i]:
                            score = score + i
                        if both[i] in l2[i]:
                            score = score + i
                    lst.append((both[i], score))
                lst.sort(key = lambda x: x[1])
                if return_style == 'list':
                    temp = list(map(list, zip(*lst)))
                    if len(temp) > 0:
                        lst = temp[0]
                    else:
                        lst = []
            return lst
        else:
            #distance = 1
            df = pd.DataFrame(index=p1, columns=p1)
            l = {}
            p2 = []
            for i in range(len(p1)):
                sp = SciPaper(p1[i])
                l[p1[i]] = self.search(sp, distance,True)
                if l[p1[i]] == None and warnings:
                    print('No citations could be obtained for ' + p1[i] + '. The citation score for this paper will be 0.')
            if distance == 1:
                for i in range(len(p1)):
                    for j in range(i+1,len(p1)):
                        num = 0
                        for k in range(len(l[p1[j]][1])):
                            if l[p1[j]][1][k] in l[p1[i]][1]:
                                num = num + 1
                        df.loc[p1[i]][p1[j]] = num
                        df.loc[p1[j]][p1[i]] = num
            else:
                master = {}
                for i in range(1,len(p1)):
                    flatten = list(chain.from_iterable(l[p1[i]]))
                    master[p1[i]] = flatten
                for i in range(len(p1)-1):
                    for j in range(i+1, len(p1)):
                        num = 0
                        for k in range(len(1,l[p1])):
                            num = num + sum(el in master[p1[i]] for el in l[p1[k]])
                        df.loc[p1[i]][p1[j]] = num
                        df.loc[p1[j]][p1[i]] = num
            return df

       
    def citation_sim(self, max_dist, p1, p2=None, return_dois=None, warnings=True):
        '''Determines the citation similarity between papers.
        Args: p1 (string, list): a DOI or a list of DOIs.
            p2 (string, None): another DOI, or None if p2 is a list.
            max_dist(int): maximum citation distance to examine
            return_dois(Bool): If true, will also return list of DOIs.Only for if comparing two papers
            warnings=True(Bool): if true, will print warnings if citations can't be obtained.
        Returns:
            None if citations can't be obtained
            if type(p1) == string: num(float): a quantification of how similar the citations
            of the two papers are.
                Will return list with num as first item and list of DOIs with their citation score
                    as second item if return_style is True.
                    If a paper is within one citation of each paper, one is added to the
                    score. After one citation, the amount added is divided by the
                    distance (so for example, a paper two citations away would add
                    1/2 to the score).
            if type(p1) == list:
                df (pandas DataFrame): matrix containing similarities
        '''
        if type(p1) == str:
            sp1 = SciPaper(p1)
            sp2 = SciPaper(p2)
            results = self.common_cit(sp1, sp2,max_dist,'tuples',warnings)
            num = 0
            if results == None:
                return None
            for i in range(len(results)):
                num = num + len(results[i][1])
            if return_dois:
                return [num, results]
            return num
        else:
            splist = []
            for i in range(len(p1)):
                sp = SciPaper(p1[i])
                splist.append(sp)
            df = pd.DataFrame(index=p1,columns=p1)
            for i in range(len(p1)-1):
                for j in range(i+1,len(p1)):
                    results = self.common_cit(splist[i],splist[j], max_dist,'list',warnings)
                    df.loc[p1[i],p1[j]] = len(results)
                    df.loc[p1[j],p1[i]] = len(results)
            return df

class TextSearcher(object):
    '''Object that uses cosine similarity to find the most relevant document.'''

    def search(self, query, num_return=None,warnings=True):#, lst=None):
        ''' Gets the cosine similarity of each item in the list and returns a list of the items by relevance.
            Args:
                query(str, SciPaper, or list of strings): the query - what we are searching for documents related to
                num_return=None(int): cap on number of results to return
                warnings=True(Bool): if True, will trigger warnings on text_sim if metadata can't be obtained.
            Returns:
                score_list(list): if list: a list of tuples, DOI then score.
            '''
        scores = []
        if type(query) != list or len(query) == 1:
            doi = query
            if type(query) == SciPaper:
                doi = query.attrs['DOI']
            elif type(query) == list:
                query = query[0]
            scores = self.text_sim(doi,warnings) #dict
        else:
            lsts = [] 
            dois = []
            for i in range(len(query)):
                results = self.text_sim(query[i], warnings)
                dois = list(results.keys())
                lsts.append(list(results.values())) #list of lists of scores
            lsts_flipped = [list(x) for x in zip(*lsts)] #now each list is for same DOI
            for i in range(len(lsts_flipped)):
                s = sum(lsts_flipped[i])
                tup = (dois[i],s)
                scores.append(tup)
        if num_return == None:
            return scores
        else:
            papers = []
            nums = [el[1] for el in scores]
            for i in range(num_return):
                max1 = max(nums)
                ind = nums.index(max1)
                papers.append(scores[ind][0])
                scores.pop(ind)
                nums.pop(ind)
            return papers
        
    def text_sim(self, p1, p2=None,warnings=False,pull=False):
        '''Quantifies text similarity of papers using cosine similarity.
            Args: p1 (doi, SciPaper or list of DOIs): first paper or list of papers
                p2 (doi, SciPaper or list): second paper or None
                warnings=True(Bool): if True and can't obtain metadata, will print warning
                pull=False (Bool): whether or not to pull new data from the Internet
            Returns: num (int): Cosine similarity if p1 and p1 are str.
                    sims (dict): DOIs keys, cosine similarity as values for each value in
                        p2 if p2 is list
                    returns df (pandas Dataframe) if p1 is list. 
        '''
        corpus = Corpus()
        sw = stopwords.words('english')
        if p2 == None:
            return 0
        #sw = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
        if ((type(p1) != list and type(p2) != list) or (type(p1) == list and len(p1) == 1)):
            str1 = ''
            str2 = ''
            sp1 = p1
            sp2 = p2
            ent = corpus.get_entry(p1)
            if type(p1) == list:
                p1 = p1[0]
                sp1 = SciPaper(p1)
                if ent == None and pull:
                    sp1.attrs = sp1.get_data()
                else:
                    sp1.attrs = ent
            elif type(p1) == str:
                sp1 = SciPaper(p1)
                if ent != None:
                    sp1.attrs = ent
                elif pull:
                    sp1.attrs = sp1.get_data()
            else:
                p1 = sp1.attrs['DOI']
            ent = corpus.get_entry(p2)
            if type(p2) == str:
                sp2 = SciPaper(p2)
                if ent == None and pull:
                    sp2.attrs = sp2.get_data()
                else:
                    sp2.attrs = ent
            else:
                p2 = sp2.attrs['DOI']
            if sp1.attrs['abstract'] == None or sp1.attrs['abstract'] == '':
                str1 = sp1.attrs['title']
            else:
                str1 = sp1.attrs['abstract']
            if sp2.attrs['abstract'] == None or sp2.attrs['abstract'] == '':
                str2 = sp2.attrs['title']
            else:
                str2 = sp2.attrs['abstract']
            if warnings:
                if str1 == None or str1 == '':
                    print('Metadata could not be obtained for ' + sp1.attrs['DOI'] + '. Please ensure that it is ' +
                              ' spelled correctly or try loading metadata in from Web of Science.')
                    return None
                if str2 == None or str2 == '':
                    print('Metadata could not be obtained for ' + sp2.attrs['DOI'] + '. Please ensure that it is ' +
                              ' spelled correctly or try loading metadata in from Web of Science.')
                    return None
            a = set(str1.split()) 
            b = set(str2.split())
            a = set([i for i in a if i not in sw])
            b = set([i for i in b if i not in sw])
            c = a.intersection(b)
            return float(len(c)) / (len(a) + len(b) - len(c))
        elif type(p2) == list: #p1 is single paper, compared against list
            str1 = ''
            sims = {}
            sp1 = p1
            if type(p1) == str:
                sp1 = SciPaper(p1)
            else:
                p1 = p1.attrs['DOI']
            if corpus.get_entry(p1) == None:
                sp1.attrs = sp1.get_data()
            else:
                sp1.attrs = corpus.get_entry(p1)
            if sp1.attrs == None:
                sims = dict(zip(p2, len(p2)*[0]))
                return sims
            elif sp1.attrs['abstract'] == None or sp1.attrs['abstract'] == '':
                str1 = sp1.attrs['title']
            else:
                str1 = sp1.attrs['abstract']
            if str1 == None or str1 == '':
                if warnings:
                    print('Metadata could not be obtained for ' + p1 + '. Please ensure that ' + p1 +' is ' +
                              ' spelled correctly or try loading metadata in from Web of Science.')
                return None
            str2 = ''
            one = word_tokenize(str1)
            lst1 = {w for w in one if not w in sw}
            for i in range(len(p2)):
                sp = SciPaper(p2[i])
                if corpus.get_entry(p2[i]) == None:
                    sp.attrs = sp.get_data()
                else:
                    sp.attrs = corpus.get_entry(p2[i])
                if sp.attrs == None or sp.attrs == {}:
                    sims[p2[i]] = 0
                    continue
                elif sp.attrs['abstract'] == None or sp.attrs['abstract'] == '':
                    if sp.attrs['title'] == None or sp.attrs['title'] == '':
                        sims[p2[i]] = 0
                    str2 = sp.attrs['title']
                else:
                    str2 = sp.attrs['abstract']
                if str2 == None:
                    sims[p2[i]] = 0
                    if warnings:
                        print('Metadata could not be obtained for ' + p2[i] + '. Please ensure that ' + p2[i] + ' is ' +
                                  ' spelled correctly or try loading metadata in from Web of Science. The cosine similarity for '+
                                  p2[i] + ' will be set to 0.')
                    continue
                two = word_tokenize(str2)
                lst2 = {w for w in two if not w in sw}
                l1 = []
                l2 = []
                rvector = lst1.union(lst2)
                for w in rvector:
                    if w in lst1:
                        l1.append(1)
                    else:
                        l1.append(0)
                    if w in lst2:
                        l2.append(1)
                    else:
                        l2.append(0)
                c = 0
                for k in range(len(rvector)):
                    c = c + l1[k]*l2[k]
                f = float((sum(l1)*sum(l2))**0.5)
                if f != 0:
                    c = c/f
                sims[p2[i]] = c#osine
                str2 = ''
            return sims
        else: #list
            df = pd.DataFrame(index=p1, columns=p1)
            sps = []
            strs = []
            for i in range(len(p1)):
                sp = SciPaper(p1[i])
                if corpus.get_entry(p1[i]) == None:
                    sp.attrs = sp.get_data()
                else:
                    sp.attrs = corpus.get_entry(p1[i])
                if sp.attrs['abstract'] == None or sp.attrs['abstract'] == '':
                    title = sp.attrs['title']
                    if title == None or title == '':
                        strs.append('')
                        if warnings:
                            print('Metadata could not be obtained for ' + p1[i] + '. Please ensure that ' + p1[i] + ' is ' +
                                  ' spelled correctly or try loading metadata in from Web of Science. The cosine similarity for '+
                                  p1[i] + ' will be set to 0.')
                    else:
                        strs.append(title)
                else:
                    strs.append(sp.attrs['abstract'])
            for i in range(len(p1)):
                for j in range(i+1,len(p1)):
                    str1 = strs[i]
                    str2 = strs[j]
                    one = word_tokenize(str1)
                    two = word_tokenize(str2)
                    lst1 = {w for w in one if not w in sw}
                    lst2 = {w for w in two if not w in sw}
                    l1 = []
                    l2 = []
                    rvector = lst1.union(lst2)
                    for w in rvector:
                        if w in lst1:
                            l1.append(1)
                        else:
                            l1.append(0)
                        if w in lst2:
                            l2.append(1)
                        else:
                            l2.append(0)
                    c = 0
                    for k in range(len(rvector)):
                        c = c + l1[k]*l2[k]
                    cosine = c/float((sum(l1)*sum(l2))**0.5)
                    df.loc[p1[i]][p1[j]] = cosine
                    df.loc[p1[j]][p1[i]] = cosine
            return df
        
class Grapher(object):
    '''Wrapper class that graphs data using Matplotlib.
    '''
    def __init__(self):
        '''Initializes variables.
        '''
        self.annot = None
        self.text = []
        self.fig = None
        self.ax = None
        self.sc = None
        self.data = [[],[],[]]

    def update_annot(self,ind):
        pos = self.sc.get_offsets()[ind["ind"][0]]
        self.annot.xy = pos
        textt = "{}".format(" ".join([self.text[n] for n in ind["ind"]]))
        self.annot.set_text(textt)
        self.annot.get_bbox_patch().set_alpha(0.4)

    def hover(self,event):
        vis = self.annot.get_visible()
        if event.inaxes == self.ax:
            cont, ind = self.sc.contains(event)
            if cont:
                self.update_annot(ind)
                self.annot.set_visible(True)
                self.fig.canvas.draw_idle()
            else:
                if vis:
                    self.annot.set_visible(False)
                    self.fig.canvas.draw_idle()

    def set_data(self, network, query_paper, distance, num_show=10000,halves=True):
        '''Processes data to graph as a scatter plot. Score is on x axis, citation data is on y axis.
            Args: network(NetworkX Graph): the citation network
                query_paper(SciPaper): search query
                distance(int): citation distance
                num_show=10000(int): number of records to show
                halves (Bool): whether or not to check if articles are included in multiple citation levels
            Returns: self.data(list of lists of length 3): graph data. DOI, citation distance, score.
                    if self.data is None or [], returns lists(2D list)
        '''
        lists = network.search(query_paper, distance,True,halves=halves)
        if lists == None:
            return None
        self.data = []
        ts = TextSearcher()
        n = 0
        if not halves:
            for i in range(1,len(lists)):
                num = (1.0/i)
                te = ts.text_sim(query_paper, lists[i])
                if type(te) == int:
                    te = [te]
                dict_scores = dict(te)
                for j in range(len(lists[i])):
                    try:
                        self.data.append([lists[i][j],num, dict_scores[lists[i][j]]])
                    except KeyError:
                        pass
                        continue
        else:
            num = 0.5
            for i in range(1,len(lists)):
                te = ts.text_sim(query_paper, lists[i])
                if type(te) == int:
                    te = [te]
                dict_scores = dict(te)
                cnum = 1/num
                for j in range(len(lists[i])):
                    self.data.append([lists[i][j],cnum, dict_scores[lists[i][j]]])
                if len(self.data) >= num_show:
                    break
                num = num + 0.5

        if self.data == [] or len(self.data) == 0:
            return lists
        elif len(self.data) > num_show:
            self.data = self.data[::-1]
            self.data = self.data[-num_show:]
        return self.data

    def graph(self):
        '''Graphs data as a scatter plot. Score is on x axis, citation data is on y axis.
            Args: None
            Returns: None
        '''
        norm = plt.Normalize(1,4)
        temp = list(map(list, zip(*self.data)))
        self.text = temp[0]
        self.fig,self.ax = plt.subplots()
        self.sc = plt.scatter(temp[1],temp[2],c='black',s=10,alpha=0.7,norm=norm)
        plt.ylabel('Cosine Similarity')
        plt.xlabel('Citation Similarity')
        #self.fig.canvas.set_window_title('Relevance')
        self.annot = plt.annotate('',xy=(0,0), xytext=(20,20),textcoords="offset points",
                             bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
        self.annot.set_visible(False)
        self.fig.canvas.mpl_connect("motion_notify_event", self.hover)
        plt.show()

def refs_in_library(doi, filepath, show=True):
    '''Tells which references of a paper are in Mendeley library. Mendeley library must be exported as one document.
        Args:
            doi (str): DOI or paper
            filepath (str): path to .bib file
            show=True (Bool): whether or not to show result graphically
        Returns:
            lib_sorted (dict): {True: [elements from list that are in library],
                False:[elements from list not in library]}    
    '''
    n = Network()
    refs = n.get_refs(doi)

    lib_sorted = in_library(refs, filepath)
    
    if show:
        corpus = Corpus()
        for i in range(len(refs)):
            temp = corpus.get_entry(refs[i])
            if refs[i] in lib_sorted[True]:
                if temp != None:
                    print(colored(temp,color='green'))
                else:
                    print(colored(refs[i], color='green'))
            else:
                if temp != None:
                    print(colored(temp,color='red'))
                else:
                    print(colored(refs[i], color='red'))

    return lib_sorted
    

def in_library(lst, filename,refs=False):
    '''Checks if a paper (or list of papers) is in a library.
        Note: Only works for publications with DOIs.
        Args:
            lst (str or list of strs): DOI or list of DOIs
            filename (str): path to library
                library must be .txt file either JSON or one DOI per line or BibTex
            refs=False (Bool): If the list of papers to be checked is the
                reference list of the paper. In that case, lst must be str.
        Returns:
            lib (dict): {True: [elements from list that are in library],
                False:[elements from list not in library]}     
    '''
    lst = [el.lower() for el in lst]
    lib = {True:[],False:[]}
    if filename == None:
        print('Error: no filename provided')
        return None
    library = []
    if type(lst) == str and not refs:
        lst = [lst]
    file = open(filename, 'r')
    f = file.read()
    file.close()
    if '.bib' in filename: #bibtex
        lines = f.split('@article{')
        for i in range(len(lines)):
            l = lines[i].lower()
            try:
                d = l.index('doi')
            except ValueError: #no doi
                pass
                continue
            chunk = l[d:]
            chunk = chunk[:chunk.index(',')]
            if '10.' in chunk:
                chunk = chunk[chunk.index('10.'):]
                if '}' in chunk:
                    doi = chunk[:chunk.index('}')]
                elif ' ' in chunk:
                    doi = chunk[:chunk.index(' ')]
            library.append(doi)
    else: #not bibtex
        try:
            library = json.loads(f) #json
        except ValueError:
            pass #not json
            library = f.splitlines()
    if refs:
        doi = lst
        n = Network()
        lst = n.get_refs(doi)
    lib[True] = [doi for doi in lst if doi in library]
    lib[False] = [doi for doi in lst if doi not in library]
    return lib

def recommend_papers(dois, citation_weight=-1, return_style=None, num_return=100, ignore_list=None, max_dist=1, warnings=True,pull=False):
    '''given a DOI or a list of DOIs as well as some other data,
        returns either a list of DOIs or a dictionary with DOIs as keys and
        a dictionary of paper data as values.
        Args: dois (string or list): if string, name of file containing DOIs
                        if list, list of DOIs (list of strings)
            citation_weight(float): how citations will weigh in the results.
                    can be -1 for root mean square of citation and text similarity values.
                    otherwise MUST be between 0.0 and 1.0. If 0.0 it is HIGHLY RECOMMENDED to cap the number returned, and this
                    may take a very long time.
            return_style(string): must be either 'dois', 'info' or list of attributes. the elements the list can contain
                    are: 'DOI', 'author', 'year', 'abstract', 'journal', 'citations'
            num_return(int): number of papers to return
            ignore_list(list): list of DOIs to ignore
            warnings=True(Bool): If True and metadata can't be retrieved, will print a warning.
            pull=False (Bool): Tells TextSearcher not to pull from the Internet
        Returns:
            papers(list or dict), depending on value of return_style. if list,
                will be list of one string(DOI), unless citation_weight is 1.0 in which case it will be
                as list of multiple strings. if dictionary will have DOIs as keys
                and paper info (dicts) as values. 
    '''
    if num_return != None and num_return < 1:
        return None
    if ignore_list == None:
        ignore_list = []
    papers = []
    scores = {} #keys are dois of papers in citation graphs, values are lists with [cit_score, topic_score]
    query_list = []
    if type(dois) == str: #if reading from file, gets contents
        if '10.' not in dois:
            file = open(dois,'r')
            query_list = file.readlines()
            file.close()
            for i in range(len(query_list)):
                if query_list[i][0] == '\n':
                    query_list[i] = query_list[i][(query_list[i].index('\n')+1):]
                elif query_list[i][-1] == '\n':
                    query_list[i] = query_list[i][:-1]
        else:
            query_list = [dois]
    else:
        query_list = query_list + dois
    if citation_weight > 0 or citation_weight == -1:
        nx_results = {}
        n = Network()
        all_list = [] #all dois
        for i in range(len(query_list)):
            sp = SciPaper(query_list[i])
            if ignore_list == None or len(ignore_list) == 0:
                temp = n.search(sp, max_dist, True,None,warnings,pull=pull)
            else:
                temp = n.search(sp, max_dist, True, ignore_list,warnings,pull=pull)
            if temp != None:
                nx_results[query_list[i]] = temp[1] #+ temp[2]
                all_list = all_list + (temp[1])# + temp[2])
            else:
                nx_results[query_list[i]] = []
        already_searched = []
        for i in range(len(all_list)):
            if all_list[i] not in already_searched and all_list[i] not in ignore_list:
                scores[all_list[i]] = all_list.count(all_list[i])#[all_list.count(all_list[i])] #!!!!!!!!!!!!!!!
                already_searched.append(all_list[i])
        if scores == {}:
            all_list = list(set(all_list))
            scores = dict(zip(all_list, [0]*len(all_list)))
        if citation_weight < 1.0:
            ts = TextSearcher()
            for i in range(len(query_list)):
                temp = ts.text_sim(query_list[i], nx_results[query_list[i]],warnings,pull)
                temp = list(zip(temp.keys(),temp.values()))
                for j in range(len(temp)): 
                    if type(scores[temp[j][0]]) == list:
                        if len(scores[temp[j][0]]) > 0:
                            scores[temp[j][0]] = scores[temp[j][0]] + [temp[j][1]]
                        else:
                            scores[temp[j][0]] = []
                            scores[temp[j][0]].append(temp[j][1])#1)
                    else:
                        scores[temp[j][0]] = [scores[temp[j][0]],temp[j][1]]#1]
    elif citation_weight == 0: #citation_weight = 0 - all text 
        ts = TextSearcher()
        scores = ts.search(query_list, warnings=warnings)
    if citation_weight == -1: #root mean square
        keys = list(scores.keys())
        for i in range(len(keys)):
            if len(scores[keys[i]]) > 1:
                rms_score = math.sqrt((((scores[keys[i]][0])**2) + (scores[keys[i]][1])**2)/2) #root mean square
                scores[keys[i]] = rms_score
            else:
                scores[keys[i]] = scores[keys[i]][0]
    elif citation_weight != 0 and citation_weight != 1: #gets weighted averages of scores
        keys = list(scores.keys())
        for i in range(len(keys)):
            if type(scores[keys[i]]) == list: #[cit score, text score] #[cit score, [text scores],num]
                if len(scores[keys[i]]) > 1:
                    avg_cit_score = scores[keys[i]][1]#sum(scores[keys[i]][1])/scores[keys[i]][2]
                    netscore = (scores[keys[i]][0]*citation_weight) + (avg_cit_score*(1-citation_weight))
                    scores[keys[i]] = netscore
                else:
                    scores[keys[i]] = scores[keys[i]][0]*citation_weight
    if num_return != None:
        if type(scores) == dict:
            scores = list(scores.items())
        scores.sort(key = lambda x: x[1])
        if num_return < len(scores):
            scores = scores[:num_return]
        papers = [e[0] for e in scores]
    else:
        if type(scores) != dict:
            scores = dict(scores)
        papers = list(scores.keys())
    if type(return_style) != list and return_style != 'info':#return_style == 'dois' or type(return_style) == None:
        return papers
    elif return_style == 'info':
        corpus = Corpus()
        paper_data = {}
        papers = [el[0] for el in scores]
        for i in range(len(papers)):
            try:
                if papers[i] not in ignore_list and papers[i] not in query_list:
                    if papers[i] in keys:
                        paper_data[papers[i]] = corpus.get_entry(papers[i])
                    else:
                        sp = SciPaper(papers[i])
                        sp.get_data()
                        paper_data[papers[i]] = sp.attrs
                #else:
                #    papers.remove(papers[i])
            except IndexError:
                pass
        return paper_data
    elif type(return_style) == list:
        corpus = Corpus()
        paper_data = {}
        papers = [e[0] for e in scores]
        for i in range(len(papers)):
            if papers[i] not in query_list:
                attrs = {}
                g = corpus.get_entry(papers[i])
                if g != None:
                    attrs = g
                else:
                    sp = SciPaper(papers[i])
                    sp.get_data()
                    attrs = sp.attrs
            temp = {}
            for j in range(len(return_style)):
                try:
                    temp[return_style[j]] = attrs[return_style[j]]
                except KeyError:
                    pass
                    continue
            paper_data[papers[i]] = dict(temp)
        return paper_data
    elif return_style == 'dois':
        return [e[0] for e in scores]

def quantify_similarity(p1, p2=None, cit_weight=-1, max_dist=1,scale=False,warnings=False):
    '''Returns a score representing how related two papers are.
        Args:
            p1 (str or list): DOI or list of DOIs
            p2=None(str or list): DOI, list of DOIs, or None
            cit_weight (float): percentage of score represented by citations. -1 for
                root mean square of citation and text scores. Otherwise must be between 0 and 1
            max_dist=1 (int): maximum citation distance, not greater than 10
            scale=False(Bool): whether or not to scale DataFrame/dict
            warnings=True (Bool): whether or not to bring up search warnings 
        Returns: num (float): relevance score (if p2 is str)
                OR
            scores (dict): dict of relevance scores (if p2 is list)
                OR
            df (pandas DataFrame): relevance score or pandas DataFrame of DOIs with scores (if p1 is list)
    '''
    token = ''
    if max_dist > 10:
        max_dist = 10
    elif max_dist < 1:
        max_dist = 1
    n = Network()
    ts = TextSearcher()
    if p2 != None and ((type(p2) == str and '10.' not in p2)  or (type(p2) == list and len(p2) == 1)):
        if type(p2) == list:
            p2 = p2[0]
        cit_score = n.citation_sim(p1, p2, max_dist,warnings)
        topic_score = ts.text_sim(p1, p2, warnings)
        if cit_score == None and topic_score == None:
            if warnings:
                print('Metadata could not be obtained. Please try again or load metadata in manually.')
            return 0
        final = 0
        if cit_weight != -1:
            final = (cit_score*cit_weight) + (topic_score*(1 - cit_weight))
        else:
            final = math.sqrt(((cit_score ** 2) + (topic_score ** 2))/2)
        return final
    elif type(p1) == str and type(p2) == list:
        scores = {}
        if cit_weight != 0:
            cit_scores = []
            for i in range(len(p2)):
                cit_scores.append(n.citation_sim(max_dist,p1,p2[i],warnings))
            max_cit = max(cit_scores)
            min_cit = min(cit_scores)
            if cit_weight != 1:
                text_scores = []
                for i in range(len(p2)):
                    text_scores.append(ts.text_sim(p1, p2[i],warnings))
                while None in text_scores:
                    text_scores[text_scores.index(None)] = 0
                if scale == False:
                    if cit_weight != -1:
                        for i in range(len(p2)):
                            scores[p2[i]] = (cit_weight*cit_scores[i]) + ((1-cit_weight)*text_scores[i])
                    else:
                        for i in range(len(p2)):
                            scores[p2[i]] = sqrt(((cit_scores[i] ** 2) + (text_scores[i] ** 2))/2)
                    return scores
                max_text = max(text_scores)
                min_text = min(text_scores)
                cit_score = 0
                text_score = 0
                if cit_weight != -1:
                    for i in range(len(p2)):
                        temp = cit_scores[i]
                        if temp == 0:
                            cit_score = 0
                        elif max_cit != min_cit:
                            cit_score = (temp - min_cit)/(max_cit - min_cit)
                        else:
                            cit_score = temp - min_cit
                        temp = text_scores[i]
                        if temp == 0:
                            text_score = 0
                        elif max_text != min_text:
                            text_score = (temp - min_text)/(max_text - min_text)
                        else:
                            text_score = temp - min_text
                        scores[p2[i]] = 10*((cit_weight*cit_score) + ((1-cit_weight)*text_score))
                else:
                    for i in range(len(p2)):
                        temp = cit_scores[i]
                        if temp == 0:
                            cit_score = 0
                        elif max_cit != min_cit:
                            cit_score = (temp - min_cit)/(max_cit - min_cit)
                        else:
                            cit_score = temp - min_cit
                        temp = text_scores[i]
                        if temp == 0:
                            text_score = 0
                        elif max_text != min_text:
                            text_score = (temp - min_text)/(max_text - min_text)
                        else:
                            text_score = temp - min_text
                        scores[p2[i]] = sqrt(((cit_scores[i] ** 2) + (text_scores[i] ** 2))/2)
            else:
                if scale == False:
                    return cit_scores
                for i in range(len(p2)):
                    temp = cit_scores[i]
                    if max_text != min_cit:
                        scores[p1[i]] = (temp - min_cit)/(max_text - min_cit)
                    else:
                        scores[p1[i]] = temp - min_cit
        else:
            text_scores = []
            for i in range(len(p2)):
                text_scores.append(ts.text_sim(p1, p2[i],warnings))
            if scale == False:
                return text_scores
            max_text = df.max().max()
            min_text = df.min().min()
            for i in range(len(p2)):
                temp = text_scores[i]
                if max_text != min_text:
                    scores[p1[i]] = (temp - min_text)/(max_text - min_text)
                else:
                    scores[p1[i]] = temp - min_text
        return scores
    else:
        df = pd.DataFrame(index=p1,columns=p1)
        cit_df = None
        text_df = None
        if cit_weight != 0:
            cit_df = n.citation_sim(max_dist, list(p1),warnings)
            max_cit = cit_df.max().max()
            min_cit = cit_df.min().min()
            if cit_weight == -1:
                text_df = ts.text_sim(list(p1), warnings)
                for i in range(len(p1)):
                    for j in range(len(p1)):
                        c_num = cit_df.iloc[i,j]
                        t_num = text_df.iloc[i,j]
                        df.loc[p1[i],p1[j]] = math.sqrt(((c_num**2) + (t_num**2))/2)
            elif cit_weight != 1: 
                text_df = ts.text_sim(list(p1), warnings)
                max_text = text_df.max().max()
                min_text = text_df.min().min()
                cit_score = 0
                text_score = 0
                for i in range(len(p1)):
                    for j in range(len(p1)):
                        temp = cit_df.iloc[i,j]
                        if temp == 0:
                            cit_score = 0
                        elif max_cit != min_cit:
                            cit_score = (temp - min_cit)/(max_cit - min_cit)
                        else:
                            cit_score = temp - min_cit
                        temp = text_df.iloc[i,j]
                        if temp == 0:
                            text_score = 0
                        elif max_text != min_text:
                            text_score = (temp - min_text)/(max_text - min_text)
                        else:
                            text_score = temp - min_text
                        df.loc[p1[i],p1[j]] = 10*((cit_weight*cit_score) + ((1-cit_weight)*text_score))
            else: 
                for i in range(len(p1)):
                    for j in range(len(p1)):
                        if i != j:
                            temp = cit_df.iloc[i,j]
                            if temp != 0:
                                if max_cit != min_cit:
                                    df.iloc[i,j] = (temp - min_cit)/(max_cit - min_cit)
                                else:
                                    df.iloc[i,j] = temp - min_cit
                            else:
                                df.iloc[i,j] = 0
        else: 
            df = ts.text_sim(list(p1), warnings)
            max_text = df.max().max()
            min_text = df.min().min()
            for i in range(len(p1)):
                for j in range(len(p1)):
                    temp = df.loc[p1[i],p1[j]]
                    if max_text != min_text:
                        df.loc[p1[i],p1[j]] = (temp - min_text)/(max_text - min_text)
                    else:
                        df.loc[p1[i],p1[j]] = temp - min_text
        return df

def thumbs(dois, cit_weight=0.5, return_style=None,warnings=True):
    '''User gives a DOI or list of DOIs. This function spits out one
        DOI at a time. The user gives it a 1 for a 'thumbs up' and a
        0 for a 'thumbs down', and the function suggests another DOI based
        on that feedback. This continues until the user tells the function to quit.
        Args:
            dois (str or list of str): query DOI or list of query DOIs
            cit_weight (float), default = 0.5: percent weight that citations take in finding related DOIs
                cit_weight can be -1 for root mean square of citation and text values
            return_style (str or list of str), default = None: return_style, either 'dois','info', or list of attrs
            warnings(Bool): whether or not to bring up search warnings
        Returns: None but prints a bunch of stuff
    '''
    if return_style == None:
        return_style = 'dois'
    current = recommend_papers(dois, cit_weight, return_style, 1,None,1,warnings)#[0] #recommend_papers(dois, citation_weight, return_style=None, num_return=None, ignore_list=None, max_dist=1, warnings=True)
    previous = []
    if type(current) == str:
        previous = [current]
    else:
        previous.append(list(current.values())[0]['DOI'])
    ignore = []
    inpu = None
    while inpu != 'q':
        if type(current) == dict:
            current = list(current.values())[0]['DOI']
        print(current)
        c = input('Enter w to view the website of the paper. Enter c to continue. Enter q to quit.')
        while c != 'w' and c != 'c':
            if c == 'q':
                return None
            c = input('Enter w to view the website of the paper. Enter c to continue. Enter q to quit.')
        if c == 'w':
            base_url = 'http://doi.org/'
            url = base_url + current
            webbrowser.open(url)
        inpu = input('Enter 1 for a \'thumbs up\', 0 for a \'thumbs down\', or q to quit.')
        while inpu != '1' and inpu != '0':
            if inpu == 'q':
                break
            inpu = input('Enter 1 for a \'thumbs up\', 0 for a \'thumbs down\', or q to quit.')
        if inpu == 'q':
            break
        if inpu == '1':
            current = recommend_papers(previous, 0.5, return_style,1)
            current = list(current.values())[0]['DOI']
            previous.append(current)
        else:
            ignore.append(current)
            previous.remove(current)
            num = -1
            new = previous[num]
            while (-1*num) < len(previous):#not in ignore and (-1*num) < len(previous):
                new = previous[num]
                num = num - 1
            if num == len(previous):
                current = recommend_papers(previous, 0.5, return_style,1)
                current = list(current.values())[0]['DOI']
            else:
                i = 0
                reclist = recommend_papers(new, 0.5, return_style, 2,ignore)
                temp = reclist[0]
                while temp not in ignore and i < len(reclist):
                    temp = reclist[i]#previous
                    i = i + 1
                if i == (len(reclist)-1):
                    print('Error. Please try again')
                    return None
            previous.append(current)

def get_metadata(paper, lst=None):
    '''Returns metadata of a paper or list of papers.
        Args:
            paper(str or list): List of DOIs(strs) if list,
                DOI or filepath to file containing DOIs. If
                filepath, filepath must not contain '10.' and
                file must be one DOI on each line.
            lst=None (list): list of metadata items to return.
            If None, returns all. Possible items on list include
                'DOI', 'title','author', 'year', 'abstract', 'journal', 'citations'
        Returns:
            m (dict): keys are DOI(s) in paper, values are dicts with names of
                metadata items as keys
    '''
    if type(paper) == str:
        if '10.' not in paper: #filepath
            file = open(paper, 'r')
            lines = file.readlines()
            file.close()
            for i in range(len(lines)):
                if '\n' in lines:
                    lines[i] = lines[i][:lines[i].index('\n')]
        else:
            paper = [paper]
    corpus = Corpus()
    m = {}
    if lst == None:
        for i in range(len(paper)):
            temp = corpus.get_entry(paper[i])
            if temp != None:
                m[paper[i]] = temp
            else:
                sp = SciPaper(paper[i])
                sp.get_data()
                m[paper[i]] = sp.attrs
    else:
        names = list(corpus.get_entry(paper[0]).keys()) #metadata types in Corpus
        ll = [x for x in lst if x in names]#metadata in both Corpus and lst
        for i in range(len(paper)):
            temp = None
            temp = corpus.get_entry(paper[i])
            if temp == None:
                sp = SciPaper(paper[i])
                sp.get_data()
                temp = sp.attrs
            m[paper[i]] = {}
            for j in range(len(ll)):
                m[paper[i]][ll[j]] = temp[ll[j]]
    if len(paper) != 1:
        return m
    return m[paper[0]]

def get_journal_info(issn):
    '''Gets the possible metadata for a paper in a certain journal.
        Args:
            issn (str): ISSN of a journal
        Returns:
            metadata (list): list of possible metadata
                title, author, year, citations, abstract
    '''
    works = Works()
    attrs = ['title','author','published-online','published-print','abstract','reference']
    url = 'http://api.crossref.org/journals/' + issn + '/works'
    r = requests.get(url)
    page = json.loads(r.text)
    items = page['message']['items']
    keys = list(items[-1].keys())
    metadata = [a for a in attrs if a in keys]
    date = [m for m in metadata if 'published-' in m]
    if len(date) != 0:
        for i in range(len(date)):
            metadata.remove(date[i])
        metadata.append('year')
    return metadata

def cluster_results(doclist,cit_weight=0.5):
    '''Uses hierarchical agglomerative clustering to cluster a list of papers.
        Args:
            doclist (list): list of DOIs of papers to cluster
            cit_weight (float): citation weight to use when quantifying paper similarity
        Returns:
            cluster_list (list): list of lists of DOIs
    '''
    df = quantify_similarity(doclist,cit_weight=cit_weight)
    
    max_ = df.max().max() #rescale dataframe
    min_ = df.min().min()
    p = list(df.index.values)
    for i in range(len(p)):
        for j in range(i+1, len(p)):
            temp = df.iloc[i,j]
            n = 1 - ((temp - min_)/(max_ - min_))
            if n != 0:
                n = math.log(n, 10)
            df.iloc[i, j] = n
            df.iloc[j, i] = n
    df.fillna(0, inplace=True)
    
    dist_matrix = [] #linkage
    for i in range(len(df)):
        arr = []
        for j in range(1,len(df.iloc[i])):
            arr.append(df.iloc[i,j])
        dist_matrix.append(list(arr))
    X = np.asarray(dist_matrix)
    
    lst = AgglomerativeClustering().fit_predict(X)

    cluster_list = []
    nums = list(set(lst))
    for i in range(len(nums)):
        cluster_list.append([])
    

    for i in range(len(lst)):
        cluster_list[lst[i]].append(doclist[i])

    return cluster_list

def author_similarity(orcid1, orcid2):
    '''Gets number of publications two authors have in common.
    Args:
        orcid1 (str): ORCID number of an author
        orcid2 (str): ORCID number of another author
    Returns:
        num (int): number of publications in common
    '''
    base = 'https://api.crossref.org/works?filter=orcid:'
    url = base + orcid1
    r = requests.get(url)
    num = r.text.count(orcid2)
    return num

def cluster_input(dois, citation_weight, num_return=100, ignore_list=None, max_dist=1, warnings=True,pull=False):
    '''Clusters input using hierarchical agglomerative clustering and calls recommend_papers on each group.
        Args: dois (list): list of DOIs (list of strings)
            citation_weight(float): how citations will weigh in the results.
                MUST be between 0.0 and 1.0. If 0.0 it is HIGHLY RECOMMENDED to cap the number returned, and this
                may take a very long time.
            num_return(int): number of papers to return for each group
            ignore_list(list): list of DOIs to ignore
            warnings=True(Bool): If True and metadata can't be retrieved, will print a warning.
            pull=False (Bool): Tells TextSearcher not to pull from the Internet
        Returns:
            papers (list of tuplets of list): first element in tuple is query list/cluster, second
            element is recommended papers
    '''
    cr = cluster_results(dois, citation_weight)
    papers = []
    for i in range(len(cr)):
        res = recommend_papers(cr[i], citation_weight, return_style='dois', num_return=num_return, ignore_list=ignore_list, max_dist=max_dist, warnings=warnings, pull=pull)
        papers.append([cr[i], res])
    return papers


