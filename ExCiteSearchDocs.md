# ExCiteSearch

ExCiteSearch is a research paper recommender system that uses both citation similarity and text-based similarity.  
For instructions on using the main functions, see excitesearch.ipynb .

## SciPaper
SciPaper is the object that represents a scientific article.

### SciPaper.\_\_init\_\_
Constructor. Can be used to pass metadata in to a SciPaper object upon creation.

### SciPaper.add\_to\_database
Checks to see if a SciPaper is already in the corpus. If not, adds the SciPaper to the corpus.

Parameters: None  
Returns: None  

```
import ExCiteSearch as e

doi = '10.1038/s41557-020-00583-0'
sp = e.SciPaper(doi)
sp.add_to_database()
```

### SciPaper.get\_data
Retrieves metadata about a scholarly article from OpenCitations and/or Crossref depending on ease of availability. Gets abstract from Semantic Scholar.

Parameters:   None  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; self.attrs _(dictionary)_: Article metadata.  

```
import ExCiteSearch as e

doi = '10.1021/acsinfecdis.8b00140'
sp = e.SciPaper(doi)
sp.attrs = sp.get_data()
print(sp.attrs)

{'DOI': '10.1021/acsinfecdis.8b00140', 'title': 'Stable Integration and Comparison of hGrx1-roGFP2 and sfroGFP2 Redox Probes in the Malaria Parasite Plasmodium falciparum', 'author': ['Anna Katharina Schuh', 'Mahsa Rahbari', 'Kim C. Heimsch', 'Franziska Mohring', 'Stanislaw J. Gabryszewski', 'Stine Weder', 'Kathrin Buchholz', 'Stefan Rahlfs', 'David A. Fidock', 'Katja Becker'], 'year': 2018.0, 'abstract': "Studying redox metabolism in malaria parasites is of great interest for understanding parasite biology, parasite-host interactions, and mechanisms of drug action. Genetically encoded fluorescent redox sensors have recently been described as powerful tools for determining the glutathione-dependent redox potential in living parasites. In the present study, we genomically integrated and expressed the ratiometric redox sensors hGrx1-roGFP2 (human glutaredoxin 1 fused to reduction-oxidation sensitive green fluorescent protein) and sfroGFP2 (superfolder roGFP2) in the cytosol of NF54- attB blood-stage Plasmodium falciparum parasites. Both sensors were evaluated in vitro and in cell culture with regard to their fluorescence properties and reactivity. As genomic integration allows for the stable expression of redox sensors in parasites, we systematically compared single live-cell imaging with plate reader detection. For these comparisons, short-term effects of redox-active compounds were analyzed along with mid- and long-term effects of selected antimalarial agents. Of note, the single components of the redox probes themselves did not influence the redox balance of the parasites. Our analyses revealed comparable results for both the hGrx1-roGFP2 and sfroGFP2 probes, with sfroGFP2 exhibiting a more pronounced fluorescence intensity in cellulo. Accordingly, the sfroGFP2 probe was employed to monitor the fluorescence signals throughout the parasites' asexual life cycle. Through the use of stable genomic integration, we demonstrate a means of overcoming the limitations of transient transfection, allowing more detailed in-cell studies as well as high-throughput analyses using plate reader-based approaches.", 'journal': 'ACS Infectious Diseases', 'citations': ['10.1038/nm.4381', '10.1016/j.ijpara.2003.09.011', '10.1016/j.ijmm.2012.07.007', '10.1515/hsz-2014-0279', '10.1016/j.molbiopara.2015.11.002', '10.1111/j.1365-2818.2008.02030.x', '10.1111/j.1365-313X.2007.03280.x', '10.1089/ars.2009.2948', '10.1016/j.freeradbiomed.2011.08.035', '10.1038/nmeth.1212', '10.1371/journal.ppat.1003782', '10.1016/j.freeradbiomed.2017.01.001', '10.1038/nmeth904', '10.1038/nbt1172', '10.1007/s10565-011-9209-3', '10.1126/science.781840', '10.2307/3280287', '10.1016/S0140-6736(81)90414-1', '10.1371/journal.ppat.1000383', '10.1385/1-59259-793-9:263', '10.1038/s41598-017-10093-8', '10.1093/molbev/msw037', '10.1007/978-1-60761-652-8_6', '10.1016/S0140-6736(94)91142-8', '10.1016/0003-9861(59)90090-6', '10.1007/BF00268910', '10.1016/j.ab.2012.10.009', '10.1096/fj.11-187401', '10.1074/jbc.M312847200', '10.1038/ncomms14478', '10.1042/BSR20160027', '10.3389/fgene.2013.00108']}
```

### SciPaper Developer Notes
The only reason that add\_to\_database could foreseeably break is if the corpus files are moved, deleted, or altered in some way. The remedy for this would be to redownload the "data" folder.

get_ data uses the requests library and JSON to retrieve and parse paper metadata from OpenCitations, Crossref, and Semantic Scholar. This function will break if OpenCitations, CrossRef, or Semantic Scholar change the formatting of their REST APIs. The fix that would need to be made if this happens is changes to the way get_data parses the JSON from these databases.

# Corpus
Representation of ExCiteSearch's corpus. Upon initialization, opens a link to the file corpus.db .  
Corpus can write in information from a Web of Science Plain Text file.  
corpus.db is the SQL database containing all of Corpus's information.

###Corpus.add\_entry
Adds an entry in the corpus. Entry must be formatted like a SciPaper's attribute dictionary.  
  
Parameters:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; attrs _(dictionary)_ : article metadata. See SciPaper self.attrs for formatting information.  
Returns: None

```
import ExCiteSearch as e
corpus = e.Corpus()
    
attrs = {'DOI': '10.1002/anie.198709281', 'title': ' 2,7,12,17-TETRAPROPYLPORPHYCENE - COUNTERPART OF OCTAETHYLPORPHYRIN IN THE PORPHYCENE SERIES', 'author': ['E VOGEL', 'M BALCI', 'K PRAMOD', 'P KOCH', 'J LEX', 'O ERMER'], 'year': '2019', 'abstract': None, 'journal': 'ANGEWANDTE CHEMIE-INTERNATIONAL EDITION IN ENGLISH ', 'citations': ['10.1111/j.1751-1097.1986.tb04708.x', '10.1021/ja00359a012', '10.1021/ja00285a049', '10.1039/p19720002111', '10.1021/ar00127a003', '10.1021/ja01258a007', '10.1021/ja00273a010', '10.1021/ja00814a018', '10.1002/anie.198611001', '10.1039/jr9630000359', '10.1002/hlca.19850680526', '10.1021/ja00797a009', '10.1021/jo00380a051', '10.1021/ja00821a076', '10.1107/S0567740872005722', '10.1246/cl.1973.1041', '10.1021/jo00886a018', '10.1002/anie.198602571', '10.1002/anie.198709311']}

corpus.add_entry(attrs)
```
###Corpus.add\_from\_wos
Adds an entry or multiple entries from a text file downloaded from the Web of Science.  
Parameters:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; filename _(str)_ : path to text file.  
Returns: None

```
import ExCiteSearch as e
corpus = e.Corpus()
corpus.add_from_wos('savedrecs.txt')
```

###Corpus.get\_entry
Retrieves an entry from the database.  
Parameters:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doi _(str)_ : DOI of entry to be retrieved.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; row _(dict)_ : metadata retrieved

```
import ExCiteSearch as e
corpus = e.Corpus()
doi = "10.1002/anie.198711621"
ent = corpus.get_entry(doi)
```

###Corpus.edit\_entry
Edits an entry in the database.  
Parameters:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doi (str): DOI of entry to be updated. 
attrs (tup): tuple where first element is key of value to be changed, second element is what attribute will be changed to  
Returns: None. 

```
import ExCiteSearch as e
corpus = e.Corpus()
doi = "10.1002/anie.198711621"
title = "Simultaneous Primary and Secondary Coordination of a Neutral Guest within a Macrocyclic Complex of Rhodium(III)"
attrs = ('title', title)
ent = corpus.edit_entry(doi, attrs)
```


### Corpus Developer Notes
The only reason that Corpus would break is if the corpus file was moved, deleted, or altered in some way, or if Web of Science changed the way it formats its plain text files. The remedy for problems with the files would be to redownload the "data" folder. The way to fix the problem with files from Web of Science would be to change the way Corpus's constructor iterates through the file.

## Network
Network creates a graphical representation of a SciPaper and citations using the networkx package.  
network.graph is the graph containing all of Network's data.  

### Network.\_\_init\_\_
Network's constructor.  
Args: 
update(True, False, or None)  
Update is False when the graph needs to be initialized. Update is True when the graph needs to be updated. Update is None when the graph does not need to be updated or initialized.

### Network.update
Args: None  
Returns: None  
Updates the graph. 

### Network.get\_refs
Gets the references of a paper or papers.  
Args: doi (str or list): either a single DOI or list of DOIS  
Returns: citations or lst (list): list of DOIs or list of lists of DOIs
  
```
import ExCiteSearch as e
n = e.Network()
doi = "10.1002/anie.198804601"
refs = n.get_refs(doi)
print(refs)

['10.1021/ac00138a019', '10.1016/S0021-9673(00)82793-4', '10.1039/c19670000858', '10.1016/0020-1790(83)90009-4', '10.1016/0020-1790(87)90163-6', '10.1039/p19820000949', '10.1016/0022-1910(83)90102-6', '10.1007/BF00990315', '10.1021/ac00259a043', '10.1007/BF01020556', '10.1021/ac00127a034', '10.1016/S0021-9673(01)93586-1', '10.1093/chromsci/14.9.425', '10.1007/BF01945083', '10.1016/0022-1910(73)90159-5', '10.1016/0022-1910(70)90157-5', '10.1021/ac60254a038', '10.1021/ac60215a014', '10.1021/ac60203a040', '10.1021/ac60240a022', '10.1021/ac50155a096', '10.1002/anie.198707841', '10.1021/ac60301a004', '10.1016/0024-3205(80)90188-5', '10.1016/0040-4039(81)80107-4', '10.1021/ac00229a055', '10.1007/BF01012101', '10.1016/S0021-9673(00)90946-4', '10.1016/S0021-9673(01)83811-5', '10.1007/BF00987544', '10.1016/S0021-9673(01)87393-3', '10.1038/1821233a0', '10.1039/c19670000952', '10.1093/chromsci/9.1.18', '10.1021/ac60269a048', '10.1002/oms.1210220713', '10.1016/0040-4020(80)80013-5', '10.1007/BF01020678', '10.1021/ac00295a069', '10.1021/ac00257a003', '10.1177/000456328101800601', '10.1021/ac60266a013', '10.1021/ac00263a022', '10.1021/ac00289a067', '10.1021/ac00224a010', '10.1016/S0021-9673(01)91739-X', '10.1007/BF01020679', '10.1021/ac60242a004', '10.1007/BF01012074', '10.1093/chromsci/14.8.360', '10.1021/ac00285a028', '10.1002/ange.19600721603', '10.1002/cber.19600931238', '10.1007/BF01411414', '10.1016/S0040-4039(00)99067-1', '10.1016/S0021-9673(01)92105-3', '10.1016/S0021-9673(00)80381-7', '10.1021/ac50155a084', '10.1007/BF00989635', '10.1016/S0021-9673(00)93838-X', '10.1016/0009-3084(72)90031-X', '10.1093/chromsci/13.11.514', '10.1016/S0021-9673(00)81519-8', '10.1093/chromsci/23.4.171', '10.1093/chromsci/21.11.524', '10.1002/bms.1200110310', '10.1002/bms.1200080110', '10.1080/00021369.1982.10865489', '10.1007/BF01945000', '10.1016/0022-1910(75)90025-6', '10.1002/bms.1200050704', '10.1021/ac00288a004', '10.1021/ac60316a024', '10.1016/S0021-9673(01)91816-3', '10.1016/S0003-2697(76)80030-9', '10.1016/S0021-9673(00)81811-7', '10.3891/acta.chem.scand.18-1551', '10.1016/S0021-9673(01)82285-8', '10.1007/BF02886864', '10.1016/S0021-9673(00)80474-4', '10.1016/S0021-9673(00)92092-2', '10.1007/BF02534442', '10.1021/ac00285a027', '10.1002/oms.1210180103', '10.1016/S0021-9673(01)97715-5', '10.1126/science.219.4582.314', '10.1007/BF00987672', '10.1126/science.227.4694.1570', '10.1021/ja00950a019', '10.1007/BF01411416', '10.1021/ac60171a016', '10.1002/oms.1210180303', '10.1002/oms.1210190905', '10.1016/0009-3084(78)90045-2', '10.1016/S0021-9673(00)95546-8', '10.1016/0022-1910(72)90147-3', '10.1016/S0021-9673(00)95281-6', '10.1016/S0021-9673(00)82581-9', '10.1093/chromsci/10.8.528', '10.1021/ac60225a004', '10.1016/S0021-9673(00)92610-4', '10.1007/BF02531867', '10.1093/chromsci/7.3.187', '10.1016/0026-265X(87)90114-7', '10.1016/S0021-9673(00)94833-7', '10.1021/ac00138a016', '10.1093/chromsci/25.10.434', '10.1002/jhrc.1240061002', '10.1093/chromsci/6.2.126', '10.1002/jhrc.1240061212', '10.1016/0009-3084(86)90112-X', '10.1016/S0021-9673(00)99954-0', '10.1007/BF02258908', '10.1002/jhrc.1240070711', '10.1007/BF01195767', '10.1007/BF00396364', '10.1007/BF00365509', '10.1021/jo01030a078', '10.1016/S0021-9673(00)89920-3', '10.1021/ac00284a050', '10.1016/S0021-9673(00)85559-4', '10.1016/S0021-9673(00)92507-X', '10.1002/oms.1210030807', '10.1021/ac00133a028', '10.1021/ja00354a055', '10.1007/BF01012371', '10.1021/ac60345a028', '10.1007/BF00988028', '10.1002/jhrc.1240051016', '10.1016/S0021-9673(01)96518-5', '10.1021/ac00132a003', '10.1021/bi00760a025', '10.4039/Ent106281-3', '10.1021/ac60317a025', '10.1007/BF00994654', '10.1021/ac00142a013', '10.1016/S0021-9673(01)89180-9']

```

### Network.search
Finds all the DOIs within a certain citation distance of a paper.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; paper(SciPaper): the starting point  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return\_all (None or True): if None, returns the DOIs that are a certain citation distance from the query. If True, returns all DOIs within that distance.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; disregard(None or list): list of DOIs to ignore in the search. if None, includes all.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): whether or not to raise warnings if citation info can't be found  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; halves=False(Bool): whether to count 1.5 citations  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pull=True(Bool): whether or not to pull metadata from the Internet  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;papers(list): a list of dictionaries of all the DOIs within a certain citation distance with corresponding SciPaper objects

```
import excitesearch as e
n = e.Network()
doi = '10.1038/NCHEM.2863'
sp = e.SciPaper(doi)
lst = n.search(sp, 1)
print(lst)

[['10.1038/NCHEM.2863'], ['10.1073/pnas.1012741107', '10.1073/pnas.1015023108', '10.1021/ar960132q', '10.1038/ncomms6943', '10.1021/cr00020a002', '10.1039/c6cs00244g', '10.1021/jm300288g', '10.1002/anie.201611813', '10.1002/anie.201310843', '10.1039/c5cs00628g', '10.1021/acs.orglett.6b01744', '10.1039/c2cc33551d', '10.1002/anie.199508091', '10.1002/anie.201503172', '10.1126/science.1176667', '10.1111/j.1582-4934.2010.01120.x', '10.1126/science.1148597', '10.1248/cpb.60.415', '10.1021/acscatal.6b02551', '10.1002/anie.201508669', '10.1038/nature10785', '10.1039/c4cs00451e', '10.1021/ja075739o', '10.1038/NCHEM.1495', '10.1002/chem.201200244', '10.1039/c003097j', '10.1021/ja961536g', '10.1002/anie.200903708', '10.1039/b603610b', '10.1002/ejoc.201600556', '10.1021/ic2006474', '10.1021/acs.joc.6b00572', '10.1021/jacs.5b09209', '10.1021/jo000726d', '10.1021/acs.orglett.6b02050', '10.1038/NCHEM.1178', '10.1002/anie.200601490', '10.1126/science.1073540', '10.1039/b820743g', '10.1021/acs.joc.6b00778', '10.1002/anie.200801675', '10.1021/om5011512', '10.1002/anie.201605236', '10.1021/jo402195x', '10.1517/17460440902916606', '10.1055/s-0035-1561639', '10.1038/ncomms1081', '10.1002/anie.201502366', '10.1016/j.phytochem.2006.11.009', '10.1016/j.jorganchem.2016.02.035', '10.1021/ci000403i', '10.1039/c5np00121h']]
```

### Network.common\_cit
Gets the citations in common between two SciPapers.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p1 (SciPaper or list of strings): one SciPaper or a list of DOIs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p2 (SciPaper or None): another SciPaper, or None if p1 is list  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; distance (int): Maximum citation distance to search for both. Must be between 1 and 10.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return\_style='list'(str): how to return - either 'list' or 'tuples'  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True (Bool): if True, will print warnings.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; list (list): list of dois ranked by relevance or df (DataFrame) pandas DataFrame with citations in common

```
import excitesearch as e
one = e.SciPaper('10.1002/anie.198916551')
two = e.SciPaper('10.1002/anie.198709311')
n = e.Network()
distance = 1 #fastest
c = n.common_cit(one, two, distance)
print(c)

['10.1002/anie.198709281', '10.1002/anie.198602571']
```

### Network.citation\_sim
Determines the citation similarity between papers using "scores". If a paper is within one citation of each paper, one is added to the score. After one citation, the amount added is divided by the distance (so for example, a paper two citations away would add 1/2 to the score).  
Args:   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; max\_dist(int): maximum citation distance to examine    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p1 (string, list): a DOI or a list of DOIs.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p2 (string, None): another DOI, or None if p2 is a list.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return\_dois(Bool): If true, will also return list of DOIs. Only for if comparing two papers. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): if true, will print warnings if citations can't be obtained.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; None if citations can't be obtained  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if type(p1) == string: num(float): a quantification of how similar the citations of the two papers are. Will return list with num as first item and list of DOIs with their citation score as second item if return_style is True.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if type(p1) == list: df (pandas DataFrame): matrix containing similarities

```
import excitesearch as e
doi1 = '10.1002/anie.198916551'
doi2 = '10.1002/anie.198709311'
n = e.Network()
cs = n.citation_sim(1, p1, p2=doi2, return_dois=False)
print(cs)

2
```

### Network Developer Notes
Network and its functions will break if the network.csv file is corrupted. The fix for this would be to redownload network.csv.  
The get_refs function could also break if OpenCitations and/or CrossRef were to cease operation or change their formatting. The fix for this would be to update the function with the new formatting. That being said, there are times when a paper's metadata is not available through either database in which case the function will not return anything. The best remedy for this is to download that paper's metadata from Web of Science.

## TextSearcher
TextSearcher handles text-based searching by calculating the cosine similarity of articles' abstracts, when available, and titles when abstracts are not available.

###TextSearcher.search
Calculates the cosine similarity of each article in a list as compared to the articles catalogued in the corpus. Warning: this function is very slow.
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; query(str, SciPaper, or list of strings): the query  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num\_return=None(int): cap on number of results to return  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): if True, will trigger warnings on text_sim if metadata can't be obtained.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; score\_list(list): if list: a list of tuples, DOI then score.

```
import excitesearch as e
lst = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901']
ts = e.TextSearcher()
results = ts.search(lst, num_return=10, warnings=False)
print(results)

['10.1002/anie.199516331', '10.1002/anie.199201731', '10.1002/anie.198808281', '10.1063/1.437783', '10.1063/1.434027', '10.1063/1.436252', '10.1002/anie.199515021', '10.1063/1.432105', '10.1063/1.435957', '10.1063/1.437094']
```

###TextSearcher.text\_sim
Quantifies text similarity of papers using cosine similarity.  
Args: p1 (doi, SciPaper or list of DOIs): first paper or list of papers  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p2 (doi, SciPaper or list): second paper or None  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): if True and can't obtain metadata, will print warning   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pull=False(Bool): whether or not to pull metadata from the Internet  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num (int): Cosine similarity if p1 and p1 are str.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sims (dict): DOIs keys, cosine similarity as values for each value in p2 if p2 is list  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; returns df (pandas Dataframe) if p1 is list.

```
'''Ranking the documents in a list by text relevance.'''
import excitesearch as e
ts = e.TextSearcher()
doclist = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901']
#list can be longer - this is just an example
query = e.SciPaper('10.1038/NCHEM.2863')
results = ts.text_sim(query, doclist)
print(results)

{'10.1002/anie.198916551': 0.05157106231293967, '10.1002/anie.198709311': 0.0, '10.1002/anie.199013901': 0.08091133940627349}
```
### TextSearcher Developer Notes
The search function can be very slow. TextSearcher does rely upon the SciPaper and Corpus objects and will break if they break.

## Grapher
Wrapper class that graphs data using Matplotlib.

###Grapher.set\_data
Processes data to graph as a scatter-plot. Text similarity score is on x axis, citation data is on y axis.
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; network(NetworkX Graph): the citation network  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; query\_paper(SciPaper): search query  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; distance(int): citation distance  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num\_show=10000(int): number of records to show  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; halves (Bool): whether or not to check if articles are included in multiple citation levels  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; self.data(list of lists of length 3): graph data. DOI, citation distance, score.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if self.data is None or [], returns lists(2D list)

There is a code example of set\_data in the documentation for Grapher's graph() function.


###Grapher.graph
Graphs data as a scatter plot. Score is on x axis, citation data is on y axis.  
Args: None  
Returns: None


```
import excitesearch as e
n = e.Network()
doi = '10.1063/1.4975608'
query_paper = e.SciPaper(doi)
distance = 2
num_show = 80
g = e.Grapher()
results = g.set_data(n, query_paper, distance, num_show)
print(results)
g.graph() 


[['10.1063/1.4976937', 0.5, 0.12869517157249538], ['10.1016/j.carbon.2019.09.057', 0.5, 0.11171254543497637], ['10.1016/j.commatsci.2019.109345', 0.5, 0.09226039141178208], ['10.1063/1.4975121', 0.5, 0.11828940292706638], ['10.1038/NCHEM.2727', 0.5, 0.09439004018637939], ['10.1063/1.4978578', 0.5, 0.13287014966075003], ['10.1016/j.commatsci.2019.109121', 0.6666666666666666, 0.12098347962395682], ['10.1063/1.4978411', 0.6666666666666666, 0.1454320060511954], ['10.1038/NCHEM.2771', 0.6666666666666666, 0.142044164845687], ['10.1038/nchem.1925', 0.6666666666666666, 0.06492654830247076], ['10.1063/1.4977704', 0.6666666666666666, 0.13681936709158718], ['10.1038/NCHEM.2444', 0.6666666666666666, 0.07551203214910351], ['10.1038/nchem.2528', 0.6666666666666666, 0.13667808797729797], ['10.1063/1.4973607', 0.6666666666666666, 0.11910765315080327], ['10.1063/1.2176616', 1.0, 0.024196695924791362], ['10.1021/jp106378p', 1.0, 0.2469897103469371], ['10.1021/ic2018693', 1.0, 0.16513888901534737], ['10.1063/1.4944935', 1.0, 0.2918231329049255], ['10.1021/jp064661f', 1.0, 0.2583692204396165], ['10.1016/j.bpc.2006.07.002', 1.0, 0.030178582014172842], ['10.1021/jp901990u', 1.0, 0.020770324619863195], ['10.1016/B978-0-444-59440-2.00004-1', 1.0, 0.0], ['10.1073/pnas.1400675111', 1.0, 0.12489052981459677], ['10.1063/1.4872239', 1.0, 0.02337622911060922], ['10.1021/jp506120t', 1.0, 0.027297554521630706], ['10.1021/jz401931f', 1.0, 0.0], ['10.1021/ja1014458', 1.0, 0.2303742526007444], ['10.1021/jp075913v', 1.0, 0.1324188742158734], ['10.1016/j.cpc.2004.12.014', 1.0, 0.02337622911060922], ['10.1039/b001167n', 1.0, 0.0], ['10.1021/jz3008497', 1.0, 0.022633936510629633], ['10.1107/S0909049500016964', 1.0, 0.0], ['10.1103/PhysRevB.54.1703', 1.0, 0.0], ['10.1063/1.4754130', 1.0, 0.02337622911060922], ['10.1002/jcc.20495', 1.0, 0.028629916715693413], ['10.1021/acs.jpcb.5b09579', 1.0, 0.0], ['10.1063/1.439694', 1.0, 0.0], ['10.1103/PhysRevB.52.2995', 1.0, 0.0], ['10.1063/1.466363', 1.0, 0.17885461783743722], ['10.1007/s00214-005-0054-4', 1.0, 0.026135418674465834], ['10.1088/0953-8984/18/37/004', 1.0, 0.1854706311308977], ['10.1021/ct2000952', 1.0, 0.02511009804759127], ['10.1021/ic035177j', 1.0, 0.11409786186836077], ['10.1021/jp037997n', 1.0, 0.08189266356489211], ['10.1002/wcms.30', 1.0, 0.0], ['10.1021/jp511602n', 1.0, 0.2130662472685305], ['10.1103/PhysRevLett.77.3865', 1.0, 0.0], ['10.1107/S0909049505012719', 1.0, 0.026135418674465834], ['10.1039/b810017a', 1.0, 0.16296434287653336], ['10.1021/jp8001614', 1.0, 0.02337622911060922], ['10.1021/acs.jctc.5b00357', 1.0, 0.0], ['10.1021/jp501091h', 1.0, 0.18385024510356482], ['10.1016/j.cpc.2010.04.018', 1.0, 0.02511009804759127], ['10.1098/rsta.2012.0482', 1.0, 0.1306770933723292], ['10.1103/PhysRevA.38.3098', 1.0, 0.0], ['10.1063/1.2821956', 1.0, 0.14947466619093902], ['10.1063/1.4790861', 1.0, 0.02337622911060922], ['10.1063/1.470117', 1.0, 0.07417485305351736], ['10.1007/s00214-012-1247-2', 1.0, 0.10454167469786334], ['10.1021/j100308a038', 1.0, 0.0], ['10.1103/PhysRevB.37.785', 1.0, 0.028629916715693413], ['10.1016/j.cplett.2014.04.037', 1.0, 0.0], ['10.1107/S0909049512027847', 1.0, 0.0], ['10.1016/S0378-3812(01)00426-5', 1.0, 0.13657372209564406], ['10.1080/00268976.2014.905721', 1.0, 0.0], ['10.1021/ct3005613', 1.0, 0.0], ['10.1021/jz501238v', 1.0, 0.028629916715693413], ['10.1103/PhysRevLett.80.890', 1.0, 0.0], ['10.1063/1.4772761', 1.0, 0.0], ['10.1063/1.3382344', 1.0, 0.15030706275739042], ['10.1063/1.438955', 1.0, 0.028629916715693413], ['10.1063/1.4801322', 1.0, 0.0], ['10.1063/1.1288688', 1.0, 0.18107149208503706], ['10.1103/RevModPhys.72.621', 1.0, 0.0], ['10.1063/1.463940', 1.0, 0.08890433771552995], ['10.1107/S0108767394013292', 1.0, 0.0], ['10.1016/0921-4526(94)00826-H', 1.0, 0.08788534316656944], ['10.1021/jp9064906', 1.0, 0.060733224763418706], ['10.1063/1.464304', 1.0, 0.030178582014172842], ['10.1063/1.478522', 1.0, 0.026135418674465834]]
```
![Results from Grapher](./grapher_example.png)

###Grapher Developer Notes
Grapher has a handful of other functions that have been left out of this documentation because no user would ever have any reason to call them.  
Grapher does rely on Network, SciPaper, and Corpus and will break if those objects break. Grapher also relies on Matplotlib and could be affected by depreciation.

## Functions Not Associated with an Object

###in\_library
Checks if a paper (or list of papers) is in a BibTex library.  
Only works for publications with DOIs.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lst (str or list of strs): DOI or list of DOIs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; filename (str): path to library. Library must be .txt file either JSON or one DOI per line or BibTex  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; refs=False (Bool): If the list of papers to be checked is the reference list of the query paper. In that case, lst must be a String.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lib (dict): {True: [elements from list that are in library], False:[elements from list not in library]}   

```
lst = ['10.1039/d0cs00258e', '10.1039/d0cs01127d','10.1063/1.4976937', '10.1063/1.2176616','10.1039/d0cs01127d', '10.1063/1.4975121','10.1039/d0cs01127d', '10.1039/d0cs01463j']
filepath = '/path/to/file/my_library.bib'
print(in_library(lst, filepath))


{True: ['10.1039/d0cs00258e', '10.1039/d0cs01127d', '10.1039/d0cs01127d', '10.1039/d0cs01127d', '10.1039/d0cs01463j'], False: ['10.1063/1.4976937', '10.1063/1.2176616', '10.1063/1.4975121']}
```

###refs\_in\_library
Checks if a paper's references are in a BibTex Library. Only works for publications with DOIs.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doi (str): DOI or paper  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; filepath (str): path to .bib file  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; show=True (Bool): whether or not to show result graphically  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lib_sorted (dict): {True: [elements from list that are in library],False:[elements from list not in library]}  
Prints to terminal if show is true.

####refs\_in\_library Developer Notes
Relies on Network and in\_library and will break if they break.


###recommend_papers
Given a DOI or a list of DOIs as well as some other data, this function returns either a list of DOIs or a dictionary with DOIs as keys and a dictionary of paper data as values.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dois (string or list): if string, name of file containing DOIs, if list, list of DOIs (list of strings). 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; citation\_weight(float): how citations will weigh in the results. If -1 will do root mean square of citation and text similarity values, otherwise MUST be between 0.0 and 1.0. If 0.0 it is HIGHLY RECOMMENDED to cap the number returned, and this may take a very long time.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return\_style=None(string): must be either 'dois', 'info' or list of attributes. the elements the list can contain are: 'DOI', 'author', 'year', 'abstract', 'journal', 'citations'  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num\_return=100(int): number of papers to return
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ignore\_list=None(list): list of DOIs to ignore  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): If True and metadata can't be retrieved, will print a warning.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pull=False(Bool): whether or not to pull metadata from the Internet  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; papers(list or dict), depending on value of return _ style. if list, papers will be a list of one string (DOI), unless citation _ weight is 1.0 in which case it will be a list of multiple strings. if papers is a dictionary it will have DOIs as keys and paper info (dicts) as values. 

```
import excitesearch as e
lst = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901']

citation_weight = 0.75
return_style = 'dois'

#recommend_papers can also take the args num_return (int), which caps the number of results returned, and ignore_list (list), a list of DOIs to be omitted from the results.
r = e.recommend_papers(lst, citation_weight, return_style,num_return=5)
print(r)


['10.1021/j150666a011', '10.1073/pnas.1006635107', '10.1021/cr00035a004', '10.1002/anie.201001839', '10.1007/s10562-011-0632-0']
```

##### Recommend_papers Developer Notes
Recommend _ papers relies on functions from Network and TextSearcher. This function also uses the functions in Network and SciPaper that retrieve information from OpenCitations and CrossRef.  
Changes to formatting in Network and TextSearcher's search and comparison functions will cause recommend_papers to break.

### Quantify_similarity
Returns a score, list, or pandas DataFrame representing the relevance of two papers, a paper and a list of papers, or papers in a list. Relevance is quantified using Network citation\_sim and/or TextSearcher text _ sim.  
Args:    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p1 (str or list): DOI or list of DOIs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p2=None (str or list): DOI, list of DOIs, or None (if p1 is list)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cit\_weight (float): If -1 will do root mean square of citation and text similarity values, otherwise MUST be between 0.0 and 1.0 as then it is the percentage of score represented by citations.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; max\_dist=1 (int): maximum citation distance, not greater than 10  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; scale=False(Bool): whether or not to scale DataFrame/dict  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=Truie (Bool): whether or not to bring up search warnings  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Returns: num (float): relevance score (if p2 is str) OR scores (dict): dict of relevance scores (if p2 is list) OR df (pandas DataFrame): relevance score or pandas DataFrame of DOIs with scores (if p1 is list)

```
p1 = '10.1038/NCHEM.2863'
lst = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901'] 
citation_weight = 0.75
print(e.quantify_similarity(p1, lst, citation_weight, max_dist=1))


{'10.1002/anie.198916551': 0.00980392156862745, '10.1002/anie.198709311': 0.0, '10.1002/anie.199013901': 0.01639344262295082}
```
##### Quantify\_similarity Developer Notes
Quantify\_similarity relies on functions from Network and TextSearcher. This function also uses the functions in Network and SciPaper that retrieve information from OpenCitations and CrossRef.  
Changes to formatting of the returned data in Network and TextSearcher's search and comparison functions will cause quantify\_similarity to break.

### Thumbs
User gives a DOI or list of DOIs. This function spits out one DOI at a time. The user gives it a 1 for a 'thumbs up' and a 0 for a 'thumbs down', and the function suggests another DOI based on that feedback. This continues until the user tells the function to quit.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dois (str or list of str): query DOI or list of query DOIs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cit\_weight (float), default = 0.5: percent weight that citations take in finding related DOIs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return\_style (str or list of str), default = None: return style, either 'dois','info', or list of attrs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings(Bool): whether or not to bring up search warnings  
Returns: None (uses print function)

```
import excitesearch as e
doi = '10.1063/1.4975608' #can also be list
cit_weight = 0.5
return_style = 'info'
e.thumbs(doi, cit_weight, return_style)
```

##### Thumbs Developer Notes
Thumbs relies upon recommend_papers and will break if recommend _ papers or the objects and functions it relies upon break. Thumbs also may run into problems after too many iterations but this will be addressed in the next version of ExCiteSearch. 

###get_metadata
Returns metadata of a paper or list of papers.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; paper(str or list): List of DOIs(strs) if list, DOI or filepath to file containing DOIs. If filepath, filepath must not contain '10.' and file must be one DOI on each line.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lst=None (list): list of metadata items to return. If None, returns all. Possible items on list include 'DOI', 'title','author', 'year', 'abstract', 'journal', 'citations'.  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; m (dict): keys are DOI(s) in paper, values are dicts with names of metadata items as keys

```
import excitesearch as e
attrs1 = ['title','year']
doclist = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901'] 
m1 = e.get_metadata(doclist,attrs1) #gets metadata of list
print(m1)

{'10.1002/anie.198916551': {'title': ' A PYRROLOPHANEDIENE-PORPHYCENE REDOX SYSTEM', 'year': '2019'}, '10.1002/anie.198709311': {'title': ' 2,3-DIHYDROPORPHYCENE - AN ANALOG OF CHLORIN', 'year': '2019'}, '10.1002/anie.199013901': {'title': ' ISOCORROLES - NOVEL TETRAPYRROLIC MACROCYCLES', 'year': '2019'}}
```
```
attrs2 = ['title','author','journal']
doi2 = '10.1038/NCHEM.2607'
m2 = e.get_metadata(doi2, attrs2) #metadata of single DOI
print(m2)

{'title': ' Adsorbate-mediated strong metal-support interactions in oxide-supported Rh catalysts', 'author': ['John Matsubu', 'Shuyi Zhang', 'Leo DeRita', 'Nebojsa Marinkovic', 'Jingguang Chen', 'George Graham', 'Xiaoqing Pan', 'Phillip Christopher'], 'journal': 'NATURE CHEMISTRY '}
```

#####get\_metadata Developer Notes
get\_metadata is dependent on SciPaper (specifically, SciPaper's get _ data function) and the Corpus and will break if get\_data breaks and/or the corpus files become corrupted.

###get\_journal\_info
get\_journal\_info gets the available metadata for papers in a specific journal.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; issn (str): ISSN of a journal  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; metadata (list): list of possible metadata title, author, year, citations, abstract

```
print(get_journal_info('1521-3773'))

['title', 'author', 'reference', 'year']
```

#####get\_journal\_info Developer Notes
get\_journal\_info relies on CrossRef's database and will break if CrossRef goes down or changes their formatting. get\_journal\_info will also not be able to get data for a journal not indexed in CrossRef.

###cluster\_results
cluster\_results uses hierarchical agglomerative clustering on a list of papers.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doclist(list): list of DOIs of papers to cluster  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cit\_weight (float): citation weight to use when quantifying  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cluster\_list (list): list of lists of DOIs in clusters  

```
doi = '10.1016/j.carbon.2019.10.053'
r = recommend_papers(doi, 0.75)
print(cluster_results(r, 0.75))

[['10.1016/j.surfrep.2011.12.001', '10.2478/s11534-011-0096-2', '10.1103/PhysRevLett.77.3865', '10.1103/PhysRevB.54.11169', '10.1103/PhysRevB.50.17953', '10.1103/PhysRevB.89.121103', '10.1063/1.1564060', '10.1103/PhysRevLett.103.246804', '10.1103/PhysRevB.16.1746', '10.1016/j.physrep.2014.03.003', '10.1021/nl301614j', '10.1088/0022-3719/3/2/010', '10.1126/science.aad4998', '10.1016/j.nanoen.2016.08.027', '10.1016/j.apsusc.2010.10.051', '10.1039/c1ra00382h', '10.1021/nl0728874', '10.1039/c8ta04618b', '10.1016/S0920-5861(03)00378-X', '10.1088/1367-2630/12/3/033014', '10.1002/adma.201900546', '10.1021/nn502842x', '10.1021/nn302729j', '10.1063/1.4812830', '10.1103/PhysRevLett.106.216101', '10.1038/ncomms6062', '10.1016/j.carbon.2017.12.104', '10.1038/ncomms3772', '10.1103/PhysRevB.78.073401', '10.1016/j.carbon.2015.05.041', '10.1103/PhysRevB.55.9520', '10.1088/2053-1583/2/3/030204', '10.1016/S0038-1098(97)00300-1', '10.1021/ja102398n', '10.1103/PhysRevB.52.7771', '10.1016/j.susc.2008.08.037', '10.1002/adma.201200564', '10.1039/c8sc03679a', '10.1021/nn1024219', '10.1016/j.nanoen.2016.09.027', '10.1039/c5nr02936h', '10.1103/PhysRevB.63.155409', '10.1038/srep11378', '10.1073/pnas.251534898', '10.1103/PhysRevLett.87.266105', '10.1002/anie.201706549', '10.1103/PhysRevLett.102.056808', '10.1088/0022-3727/49/27/275304', '10.1002/aenm.201500010', '10.1088/1367-2630/11/6/063002', '10.1039/c2nr31480k', '10.1002/adma.201602581', '10.1021/jp811020s', '10.1002/adfm.201000274', '10.1039/c8ta03404d', '10.1039/c2jm16709c', '10.1039/b718768h', '10.1021/nn800251f', '10.1002/anie.201901361', '10.1021/acs.jpcb.7b05518', '10.1002/adfm.201804284', '10.1039/c1ra00621e', '10.1002/adma.201104110', '10.1038/nmat1695', '10.1021/acs.jpcc.6b12506', '10.1088/2053-1583/aaf836', '10.1039/c1jm13490f', '10.1039/c1cs15172j', '10.1039/c3ta14108j', '10.1016/j.apcatb.2010.10.007', '10.1088/2053-1591/ab244a'], ['10.1016/j.surfrep.2011.12.001', '10.2478/s11534-011-0096-2', '10.1103/PhysRevLett.77.3865', '10.1103/PhysRevB.54.11169', '10.1103/PhysRevB.50.17953', '10.1103/PhysRevB.89.121103', '10.1063/1.1564060', '10.1103/PhysRevLett.103.246804', '10.1103/PhysRevB.16.1746', '10.1016/j.physrep.2014.03.003', '10.1021/nl301614j', '10.1088/0022-3719/3/2/010', '10.1126/science.aad4998', '10.1016/j.nanoen.2016.08.027', '10.1016/j.apsusc.2010.10.051', '10.1039/c1ra00382h', '10.1021/nl0728874', '10.1039/c8ta04618b', '10.1016/S0920-5861(03)00378-X', '10.1088/1367-2630/12/3/033014', '10.1002/adma.201900546', '10.1021/nn502842x', '10.1021/nn302729j', '10.1063/1.4812830', '10.1103/PhysRevLett.106.216101', '10.1038/ncomms6062', '10.1016/j.carbon.2017.12.104', '10.1038/ncomms3772', '10.1103/PhysRevB.78.073401', '10.1016/j.carbon.2015.05.041', '10.1103/PhysRevB.55.9520', '10.1088/2053-1583/2/3/030204', '10.1016/S0038-1098(97)00300-1', '10.1021/ja102398n', '10.1103/PhysRevB.52.7771', '10.1016/j.susc.2008.08.037', '10.1002/adma.201200564', '10.1039/c8sc03679a', '10.1021/nn1024219', '10.1016/j.nanoen.2016.09.027', '10.1039/c5nr02936h', '10.1103/PhysRevB.63.155409', '10.1038/srep11378', '10.1073/pnas.251534898', '10.1103/PhysRevLett.87.266105', '10.1002/anie.201706549', '10.1103/PhysRevLett.102.056808', '10.1088/0022-3727/49/27/275304', '10.1002/aenm.201500010', '10.1088/1367-2630/11/6/063002', '10.1039/c2nr31480k', '10.1002/adma.201602581', '10.1021/jp811020s', '10.1002/adfm.201000274', '10.1039/c8ta03404d', '10.1039/c2jm16709c', '10.1039/b718768h', '10.1021/nn800251f', '10.1002/anie.201901361', '10.1021/acs.jpcb.7b05518', '10.1002/adfm.201804284', '10.1039/c1ra00621e', '10.1002/adma.201104110', '10.1038/nmat1695', '10.1021/acs.jpcc.6b12506', '10.1088/2053-1583/aaf836', '10.1039/c1jm13490f', '10.1039/c1cs15172j', '10.1039/c3ta14108j', '10.1016/j.apcatb.2010.10.007', '10.1088/2053-1591/ab244a']]
```

#####cluster\_results Developer Notes
cluster\_results relies on Sci-Kit-Learn's Agglomerative Clustering function as well as quantify\_similarity. cluster\_results will break if quantify\_similarity returns an empty DataFrame.

###author\_similarity
Gets number of publications two authors have in common.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; orcid1 (str): ORCID number of an author  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; orcid2 (str): ORCID number of another author  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num (int) number of publications in common  

```
orcid1 = '0000-0002-4157-1745'
orcid2 = '0000-0002-4452-8240'
print(author_similarity(orcid1, orcid2))

1
```

#####author\_similarity Developer Notes
author\_similarity relies on the requests library as well as Crossref's REST API.


###cluster\_input
Clusters input using hierarchical agglomerative clustering and calls recommend\_papers on each group.  
Args:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dois (list): list of DOIs (list of strings)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; citation\_weight(float): how citations will weigh in the results. MUST be between 0.0 and 1.0. If 0.0 it is HIGHLY RECOMMENDED to cap the number returned, and this may take a very long time.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num\_return(int): number of papers to return for each group  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ignore\_list(list): list of DOIs to ignore  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warnings=True(Bool): If True and metadata can't be retrieved, will print a warning.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pull=False (Bool): Tells TextSearcher not to pull from the Internet  
Returns:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; papers (list of tuplets of list): first element in tuple is query list/cluster, second element is recommended papers

```
lst = ['10.1002/anie.198916551', '10.1002/anie.198709311', '10.1002/anie.199013901','10.1038/nchem.1412','10.1038/NCHEM.2607']
c = cluster_input(lst, 0.75,num_return=10,pull=True)
print(c)

[[['10.1002/anie.198709311', '10.1002/anie.199013901', '10.1038/nchem.1412', '10.1038/nchem.2607'], ['10.1002/anie.199013901', '10.1002/anie.198709311', '10.1002/anie.198916511', '10.1002/anie.198800051', '10.1039/jr9650001620', '10.1002/anie.198203431', '10.1039/c39740000330', '10.1021/ja00360a051', '10.1351/pac199062030557', '10.1021/ja00160a056']], [['10.1002/anie.198916551'], ['10.1002/anie.199013851', '10.1002/anie.199116931', '10.1002/anie.198709281', '10.1002/anie.198602571', '10.1002/anie.198709311', '10.1002/anie.198804111', '10.1021/ja00507a039', '10.1002/anie.198604511', '10.1039/cs9800900091', '10.1002/anie.198808653']]]
```

#####cluster\_input Developer Notes
cluster\_input relies on recommend\_papers and cluster\_results. cluster\_input's parameters are pretty much the same as recommend\_papers's parameters so changes to the parameters of recommend\_papers might cause a problem.