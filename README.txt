excitesearch.py
=================
excitesearch.py is a search engine designed for academic papers. It uses collaborative filtering as well as cosine similarity and a database.

Last updated: May 10, 2022
Created by Julia Sterling at Tulane University in collaboration with Dr. Matthew Montemore

How to use:
-----------
Open up ecs-example.py, import excitesearch.py and call functions from a script, or run it from the command line. 

Requirements:
-------------
An Internet connection
Python 3.9.2
The pandas, matplotlib, requests, networkx, and numpy Python packages as well as the Crossref REST API. These can be downloaded using pip in a bash window. There are instructions on how to do this at https://packaging.python.org/tutorials/installing-packages/ . There are other dependencies (sys, json, math, time. webbrowser, and os), but these should come with the Python install.

Features:
---------
Allows the user to decide how they would like to search as well as how many results are returned and how they are returned. Uses collaborative filtering as well as cosine text similarity (a form of text-based similarity), and can display results graphically.
Finding the common citations between two papers.
Based on a list of DOIs or a .txt file containing a list of DOIs (one on each line), recommends papers.
Quantifying the similarity between two papers or one paper and a list of papers.
Has "thumbs" functionality gives one recommendation at a time and takes feedback in real-time. 
Checks if a paper is in a bibtex file. 
ecs-example.ipynb is a Jupyter Notebook containing examples of how to use excitesearch.py.

Running From Another Script:
----------------------------
excitesearch.py and its associated scripts can be run from any other script on the same computer. The following lines of code must be run before this can be done:
	import sys
	sys.path.insert(1, 'path/to/folder/excitesearch.py/')
Then, import excitesearch like a normal package. For example, two ways of doing this might be: 
	import excitesearch as ecs
	from excitesearch import *
	from excitesearch import SciPaper

Downloading Records from Web of Science:
----------------------------------------
Detailed instructions on how to do this can be found in info.pdf . 

Limitations:
------------
Due to the sheer number of web requests involved in many of the functionalities, the module can be quite slow. For example, many papers cite important papers that have hundreds or thousands of citations, so when going more than one citation out from a paper the runtime can and will lag. One way to decrease the runtime (although only slightly) is to run the module from a script or from the command line instead of the GUI or the driver.
Citation information that is not downloaded from Web of Science is only available from OpenCitations and Crossref, which, though extensive, are not complete. If searching for records from specific journals and/or a specific time frame, downloading records from Web of Science is suggested.


If you encounter any bugs in the program, please email me at jsterling@tulane.edu !